
local Debug = CS.UnityEngine.Debug
local LuaManager = CS.LuaManager
local Instance = LuaManager.Instance
local RenderTexture = CS.UnityEngine.RenderTexture
local Texture2D = CS.UnityEngine.Texture2D
local Rect = CS.UnityEngine.Rect
local File = CS.System.IO.File
local Path = CS.System.IO.Path
local RenderSettings = CS.UnityEngine.RenderSettings
local QualitySettings = CS.UnityEngine.QualitySettings
local Camera = CS.UnityEngine.Camera
local Light = CS.UnityEngine.Light
local SmBodySkin = CS.CustomMenu.SmBodySkin
local CharFemaleCustom = CS.CharFemaleCustom
local CustomControl = CS.CustomControl
local SubMenuControl = CS.CustomMenu.SubMenuControl
local SubMenuBase = CS.CustomMenu.SubMenuBase
local SmBodyShapeD_F = CS.CustomMenu.SmBodyShapeD_F
local GameObject = CS.UnityEngine.GameObject
local SkinnedMeshRenderer = CS.UnityEngine.SkinnedMeshRenderer
local System = CS.System
local Application = CS.UnityEngine.Application
local String = CS.System.String
local Graphics = CS.UnityEngine.Graphics

Debug.LogError("FinishLoad")
print("FInishWork")
local function debugMsg( e )
	Debug.LogError(e)
	Debug.LogError("S:"..debug.traceback())
end

local function GetTex( mat , texName)
	local mt = mat:GetTexture(texName)
	local w = mt.width
	local h = mt.height
	print(mt, w, h)
	local tex2D = Texture2D(w, h)
	Graphics.DrawTexture(Rect(0, 0, w, h), mt)
	--RenderTexture.active = mt
	tex2D:ReadPixels(Rect(0, 0, w, h), 0, 0)
	tex2D:Apply()

	local svPath = Path.Combine(Application.dataPath, String.Format("../Export/{0}.png", texName))
	print(svPath)
	File.WriteAllBytes(svPath, tex2D:EncodeToPNG())

	RenderTexture.active = nil
end 

local colors = {
	"_Color",
	"_EmisColor",
	"_EmissionColor",
	"_SpecColor",
	"_TintColor",
}

local function GetColor( mat )
	for k, v in ipairs(colors) do
		print(v, mat:GetColor(v))
	end
end

local floats = {
	"_BasedSpecColor",
	"_BlendNormalMapScale",
	"_BumpScale",
	"_Cutoff",
	"_DetailNormalMapScale",
	"_DstBlend",
	"_Fresnel",
	"_Glossiness",
	"_InvFade",
	"_Metallic",
	"_Mode",
	"_OcclusionStrength",
	"_Parallax",
	--"_Shininess",
	"_SpecInt",
	--"_SrcBlend",
	"_UVSec",
	"_ZWrite",
}

local function PrintFloat( mat )
	for k,v in ipairs(floats) do
		print(v, mat:GetFloat(v))
	end
end

local tex = {
	"_BumpMap",
	"_MainTex",
	"_SpecGlossMap",
	"_OcclusionMap",
	"_DetailNormalMap",
	"_DetailMask",

}
local function PrintTex( mat )
	for k, v in ipairs(tex) do
		print(v, mat:GetTexture(v))
		--GetTex(mat, v)
	end
end

local function PrintShader( mat )
	print(mat.shader)
end
local function T(  )
	--local head = GameObject.Find("cf_O_eyewhite_R")
	local head = GameObject.Find("cf_O_eyehikari_L")
	local smr = head:GetComponent(typeof(SkinnedMeshRenderer))
	local sm = smr.sharedMaterial
	print(sm.name)
	PrintShader(sm)
	PrintTex(sm)
	PrintFloat(sm)
	GetColor(sm)
	--[[
	GetTex(sm, "_MainTex")
	GetTex(sm, "_SpecGlossMap")
	GetColor(sm)
	print("floats")
	GetFloat(sm)
	--]]
end
xpcall(T, debugMsg)
--[[
local function T(  )
	Debug.LogError("What")
	print("Hello Head123")
end


xpcall(T, debugMsg)
--]]
