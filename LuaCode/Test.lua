local Debug = CS.UnityEngine.Debug
local LuaManager = CS.LuaManager
local Instance = LuaManager.Instance
local RenderTexture = CS.UnityEngine.RenderTexture
local Texture2D = CS.UnityEngine.Texture2D
local Rect = CS.UnityEngine.Rect
local File = CS.System.IO.File
local Path = CS.System.IO.Path
local RenderSettings = CS.UnityEngine.RenderSettings
local QualitySettings = CS.UnityEngine.QualitySettings
local Camera = CS.UnityEngine.Camera
local Light = CS.UnityEngine.Light
local SmBodySkin = CS.CustomMenu.SmBodySkin
local CharFemaleCustom = CS.CharFemaleCustom
local CustomControl = CS.CustomControl
local SubMenuControl = CS.CustomMenu.SubMenuControl
local SubMenuBase = CS.CustomMenu.SubMenuBase
local SmBodyShapeD_F = CS.CustomMenu.SmBodyShapeD_F

Debug.LogError("What")
print("Hello Test")
local GameObject = CS.UnityEngine.GameObject
local SkinnedMeshRenderer = CS.UnityEngine.SkinnedMeshRenderer
local System = CS.System
local Application = CS.UnityEngine.Application

local go = GameObject.Find("cf_O_body_00")
print(go)
local sm = go:GetComponent(typeof(SkinnedMeshRenderer))
print(sm)

local mat = sm.material
print(mat)
print(mat:GetColor("_Color"))


--mat:DisableKeyword("_skin_effect")
--mat:EnableKeyword("_skin_effect")
--mat:DisableKeyword("_rimlight")

 mat:SetFloat("_skin_effect", 0)
 mat:SetFloat("_rimlight", 0)
 Test = Test or {}

--[[
local cs_coroutine = function (  )
	local util = require 'util'

	local gameobject = CS.UnityEngine.GameObject('Coroutine_Runner')
	CS.UnityEngine.Object.DontDestroyOnLoad(gameobject)
	local cs_coroutine_runner = gameobject:AddComponent(typeof(CS.Coroutine_Runner))

	local function async_yield_return(to_yield, cb)
	    cs_coroutine_runner:YieldAndCallback(to_yield, cb)
	end

	return {
	    yield_return = util.async_to_sync(async_yield_return)
	}
end

local util = require 'util'
local yield_return = cs_coroutine().yield_return
--]]

local f = System.String.Format("file://{0}/../Abs/bodyab", Application.dataPath)
print(f)
if Test.cor == nil then
	Test.cor = Instance:StartCoroutine(CS.LoadShader.LoadAb(f))
	print(Test.cor)
end

--[[
local co = coroutine.create(function()
	print('coroutine start!')
	if Test.bundle == nil then
		yield_return(CS.UnityEngine.WaitForSeconds(3))
		if CS.LoadShader.bundle == nil then
		end
		print(CS.LoadShader.bundle)
		Test.bundle = CS.LoadShader.bundle
	end
	print("LoadBundle")

	local mmn = "Assets/body2/Materials/body3.mat"
	mmn = string.lower(mmn)
	print("LoadM", mmn)
	local m = Test.bundle:LoadAsset(mmn)

	print(m.shader)
	mat.shader = m.shader
end)


assert(coroutine.resume(co))
--]]
local floats = {
	"_Metallic",
	"_Smoothness",	
	"_OcclusionStrength",
	"_DetailNormalMapScale",

}


local colors = {
	"_Color",
	"_SpecColor",
}

local texs = {
	"_BumpMap",
	"_MainTex",
	"_SpecGlossMap",
	"_OcclusionMap",
	"_DetailNormalMap",
	"_DetailMask",
}

if CS.LoadShader.bundle ~= nil then
	Test.bundle = CS.LoadShader.bundle
	print(CS.LoadShader.bundle)

	local mmn = "Assets/body2/Materials/body3.mat"
	mmn = string.lower(mmn)
	print("LoadM", mmn)
	local m = Test.bundle:LoadAsset(mmn)
	print(m.shader)

	if Test.oldShader == nil then
		Test.oldShader = mat.shader
	end
	--mat.shader = m.shader
	--mat.shader = Test.oldShader
	--mat:DisableKeyword("_skin_effect")

	print("oldShader", Test.oldShader)
	for k, v in ipairs(colors) do
		print(v, mat:GetColor(v))
	end

	for k, v in ipairs(texs) do
		print(v, mat:GetTexture(v))
	end

	for k, v in ipairs(floats) do
		print(v, mat:GetFloat(v))
	end

	--[[
	local mt = mat:GetTexture("_MainTex")
	local w = mt.width
	local h = mt.height
	print(mt, w, h)
	local tex2D = Texture2D(w, h)

	RenderTexture.active = mt
	tex2D:ReadPixels(Rect(0, 0, w, h), 0, 0)
	tex2D:Apply()

	local svPath = Path.Combine(Application.dataPath, "../Export/main.png")
	print(svPath)
	File.WriteAllBytes(svPath, tex2D:EncodeToPNG())

	RenderTexture.active = nil
	--]]

	print("RenderSettings", RenderSettings.ambientMode)
	print("RenderSettings", RenderSettings.ambientSkyColor)
	print("RenderSettings", RenderSettings.skybox)
	print("RenderSettings", RenderSettings.skybox.shader)
	print("RenderSettings", RenderSettings.sun)
	local skybox = RenderSettings.skybox
	print("sky", skybox:GetColor("__Tint"))
	print("sky", skybox:GetFloat("_Exposure"))
	print("sky", skybox:GetFloat("_Rotation"))
	print("sky", skybox:GetTexture("_Tex"))
	local cube = skybox:GetTexture("_Tex")
	print("cubeMap", cube.format)



	print("QualitySettings", QualitySettings.activeColorSpace)
	print("Camera", Camera.main.clearFlags)

	local cams = GameObject.FindObjectsOfType(typeof(Camera))
	for i=0, cams.Length-1, 1 do
		local c = cams[i]
		print("Cam", c.name, c.clearFlags, c.cullingMask, c.depth)
	end

	local lights = GameObject.FindObjectsOfType(typeof(Light))
	for i =0, lights.Length-1, 1 do
		local l = lights[i]

		print("Light", l.name, l.transform.eulerAngles, l.intensity, l.color)
	end

else
	print("NotLoadBundle")
end

--[[
local mc = Camera.main.gameObject
print("Export", mc)
local EntityConfigExport = CS.MyLib.EntityConfigExport
EntityConfigExport.ExportMethod(mc)
--]]

xlua.private_accessible(SmBodySkin)
xlua.private_accessible(CharFemaleCustom)

local bs = GameObject.FindObjectOfType(typeof(CustomControl))
print(bs)
--local cf = bs.chaCustomF
--print(cf)

local matCreate = bs.chainfo.chaBody.customTexCtrlBody.matCreate
local ct = bs.chainfo.chaBody.customTexCtrlBody

print("matCreate", matCreate)

local texs = {
	"_MainTex",
	"_Texture2",
	"_Texture3",
}
local colors = {
	"_Color",
	"_Color2",
	"_Color3",
}

print("matCreateShader", matCreate.shader)
for k, v in ipairs(texs) do
	print(v, matCreate:GetTexture(v))
end

for k, v in ipairs(colors) do
	print(v, matCreate:GetColor(v))
end

print("customTexCtrlBody", ct.texMain)

local sub = GameObject.FindObjectOfType(typeof(SubMenuControl))
print(sub)
print(sub.nowSubMenuTypeStr, sub.nowSubMenuTypeId, sub.smItem)
print(sub.smItem[sub.nowSubMenuTypeId])
local smItem = sub.smItem[sub.nowSubMenuTypeId]
local topObj = smItem.objTop
print(topObj)

local smb = topObj:GetComponent(typeof(SmBodyShapeD_F))
print(smb)

