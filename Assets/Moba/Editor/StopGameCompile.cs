﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
public class StopGameCompile
{
    static StopGameCompile()
    {
        EditorApplication.update -= StopIfRecomple;
        EditorApplication.update += StopIfRecomple;
    }

    static void StopIfRecomple()
    {
        if(EditorApplication.isCompiling && EditorApplication.isPlaying)
        {
            EditorApplication.isPlaying = false;
        }
    }
}
