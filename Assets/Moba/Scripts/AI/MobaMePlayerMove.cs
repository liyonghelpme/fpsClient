﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyLib
{
    public class MobaMePlayerMove : MoveState 
    {
        VirtualController vcontroller;
        Vector3 camRight;
        Vector3 camForward;
        IPhysicCom physics;
        ISyncInterface networkMove;

        public override void Init()
        {
            base.Init();
            var playerMove = GetAttr().GetComponent<MoveController>();
            vcontroller = playerMove.vcontroller;
            physics = GetAttr().GetComponent<IPhysicCom>();
            camRight = CameraController.Instance.camRight;
            camForward = CameraController.Instance.camForward;
        }

        public override void EnterState()
        {
            base.EnterState();
            aiCharacter.SetRun();
        }

        /// <summary>
        /// MoveController 摇杆输入
        /// MobaMeSync 网络输入
        /// 两个综合考虑
        /// </summary>
        /// <returns></returns>
        public override IEnumerator RunLogic()
        {
            networkMove = GetAttr().GetComponent<ISyncInterface>();
            var physics = GetAttr().GetComponent<CreepMeleeAI>();
            while (!quit)
            {
                //var curInfo = networkMove.GetServerVelocity();
                var isLocalMove = MobaUtil.IsLocalMove(vcontroller);
                var isNetMove = MobaUtil.IsServerMoveBySpeedOrPos(networkMove);
                if(isLocalMove || isNetMove)
                {
                    if (isNetMove)//依赖服务器寻路
                    {
                        //MoveByNet();
                        //多个服务器发过来的帧率 有可能导致预测失败 现在的位置超过了预测位置导致玩家不断回头
                        //yield return GetAttr().StartCoroutine(MoveSmooth());
                        MoveByNet();
                        yield return new WaitForFixedUpdate();
                    }
                    else if (isLocalMove) //依赖本地的寻路
                    {
                        MoveByHand();
                        yield return new WaitForFixedUpdate();
                    }
                }else
                {
                    break;
                }
            }
            if (!quit)
            {
                aiCharacter.ChangeState(AIStateEnum.IDLE);
            }
        }

        /// <summary>
        /// 自己玩家正常速度移动
        /// </summary>
        private void MoveByNet()
        {
            var netPos = networkMove.GetServerPos();
            var curPos = GetAttr().transform.position;
            var tarDir = netPos - curPos;
            tarDir.y = 0;
            var deltaDist = tarDir.magnitude;
            var speedCoff = MobaUtil.GetSpeedCoff(curPos, netPos);
            var oriSpeed = aiCharacter.GetAttr().GetSpeed() * speedCoff;
            Log.AI("MoveMe:"+curPos+":"+netPos+":"+networkMove.GetServerVelocity()+":"+networkMove.GetCurInfoPos()+":"+networkMove.GetCurInfoSpeed());
            var mdir = tarDir.normalized;
            var netDir = MobaUtil.GetNetTurnTo(vcontroller.inputVector, mdir, deltaDist, physics.transform.forward);
            physics.TurnTo(netDir);

            if (Mathf.Abs(deltaDist) < 0.1f)
            {
            }
            else
            {
                var sp = mdir * oriSpeed;
                var newPos = GetAttr().transform.position + sp * Time.fixedDeltaTime;
                physics.MoveToIgnorePhysic(newPos);
            }
        }

        private void MoveByHand()
        {
            float v = 0;
            float h = 0;
            h = vcontroller.inputVector.x; //CameraRight 
            v = vcontroller.inputVector.y; //CameraForward
            var targetDirection = h * camRight + v * camForward;
            var mdir = targetDirection.normalized;

            var localPos = physics.transform.position;
            var netPos = networkMove.GetServerPos();
            var speedCoff = MobaUtil.GetSpeedDecCoff(localPos, netPos);

            physics.TurnTo(mdir);
            var speed = aiCharacter.GetAttr().GetSpeed() * speedCoff;
            var sp = mdir* speed;
            var newPos = GetAttr().transform.position + sp * Time.fixedDeltaTime;
            physics.MoveToWithPhysic( newPos);
        }
    }
}