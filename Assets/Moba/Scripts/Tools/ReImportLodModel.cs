﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using MyLib;

public class ReImportLodModel : MonoBehaviour
{
    [System.Serializable]
    public struct PrefabInfo
    {
        public GameObject inst;
        public GameObject prefab;
        public GameObject meshModel;
        public string prefabPath;
    }


    [ButtonCallFunc()]
    public bool SimpleScene2;
    public void SimpleScene2Method()
    {
        allMesh = new List<Mesh>();
        pathes = new List<string>();
        allGo = new List<GameObject>();

        var meshes = GameObject.FindObjectsOfType<MeshRenderer>();
        foreach(var m in meshes)
        {
            if (m.gameObject.activeInHierarchy)
            {
                var mf = m.GetComponent<MeshFilter>();
                if (mf && mf.sharedMesh)
                {
                    var path = AssetDatabase.GetAssetPath(mf.sharedMesh);
                    if (!string.IsNullOrEmpty(path) && !path.Contains("Library/"))
                    {
                        allMesh.Add(mf.sharedMesh);
                        pathes.Add(path);
                        allGo.Add(mf.gameObject);
                    }
                }
            }
        }

        /*
        AssetDatabase.StartAssetEditing();
        foreach(var m in pathes)
        {
            AssetDatabase.ImportAsset(m);
        }
        AssetDatabase.StopAssetEditing();
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        */

        allPrefabs = new List<GameObject>();
        allPrefInst = new List<PrefabInfo>();
        hasPrefab = new Dictionary<string, bool>();

        //获取PrefabUtil 设置其LodGroup信息
        foreach(var g in allGo)
        {
            var root = PrefabUtility.GetNearestPrefabInstanceRoot(g);
            if (root != null)
            {
                var path = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(root);
                if (hasPrefab.ContainsKey(path))
                {
                    continue;
                }
                hasPrefab.Add(path, true);
                var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(path);
                allPrefabs.Add(prefab);

                var sm = root.GetComponentInChildren<MeshFilter>().sharedMesh;
                var path1 = AssetDatabase.GetAssetPath(sm);
                var mm = AssetDatabase.LoadAssetAtPath<GameObject>(path1);


                var prefabInfo = new PrefabInfo()
                {
                    inst = root,
                    prefabPath = path,
                    meshModel = mm,
                    prefab = prefab,
                };

                allPrefInst.Add(prefabInfo);
            }
        }

        foreach(var p in allPrefInst)
        {
            var lod = p.meshModel.GetComponent<LODGroup>();
            //var newInst = GameObject.Instantiate<GameObject>(p.meshModel);
            var newInst = p.inst;
            var setYet = newInst.GetComponent<LODGroup>();
            if(setYet)
            {
                continue;
            }
            var mr = newInst.GetComponent<MeshRenderer>();
            if (mr == null) continue;

            Debug.LogError("HandlePrefab:"+ Util.GetTreePath(p.inst));

            var newLodGroup = p.inst.AddComponent<LODGroup>();
            newLodGroup.fadeMode = lod.fadeMode;
            newLodGroup.localReferencePoint = lod.localReferencePoint;
            newLodGroup.animateCrossFading = lod.animateCrossFading;
            newLodGroup.size = lod.size;
            newLodGroup.enabled = lod.enabled;

            var lods = lod.GetLODs();
            var newLods = new LOD[lods.Length];

            //0 级别Lod
            for(var i = 0; i < lods.Length; i++)
            {
                var o = lods[i];
                var l = new LOD();
                l.screenRelativeTransitionHeight = o.screenRelativeTransitionHeight;
                l.fadeTransitionWidth = o.fadeTransitionWidth;

                var oldR = o.renderers;
                var newR = new Renderer[oldR.Length];

                if (i == 0)
                {
                    var r = newInst.GetComponent<Renderer>();
                    newR[0] = r;
                }
                else
                {
                    for (var j = 0; j < oldR.Length; j++)
                    {
                        if (i == 0)
                        {
                            //lod0 自身
                            var r = newInst.GetComponent<Renderer>();
                            newR[j] = r;
                        }
                        else
                        {
                            var newG = GameObject.Instantiate<GameObject>(oldR[j].gameObject);
                            newG.transform.parent = newInst.transform;
                            MyLib.Util.InitGameObject(newG);

                            var r = newG.GetComponent<Renderer>();
                            newR[j] = r;
                        }
                    }
                }

                l.renderers = newR;
                newLods[i] = l;
            }
            newLodGroup.SetLODs(newLods);
            //break;
        }

        foreach(var p in allPrefInst)
        {
            var lod = p.meshModel.GetComponent<LODGroup>();
            var newInst = p.inst;
            var setYet = newInst.GetComponent<LODGroup>();
            if (setYet)
            {
                var lods = setYet.GetLODs();
                var l0 = lods[0].renderers[0];

                Debug.LogError("HandleRender:" + Util.GetTreePath(p.inst));
                var screenHeight = new List<float>()
                {
                    0.08f, 0.02f, 0.01f,
                };
                for (var i = 0; i < lods.Length; i++)
                {
                    var l = lods[i];
                    var rs = l.renderers;
                    rs[0].sharedMaterial = l0.sharedMaterial;
                    l.screenRelativeTransitionHeight = screenHeight[i];
                    lods[i] = l;
                }
                Debug.Log("Lods;"+lods[1].screenRelativeTransitionHeight+":"+lods.Length);
                setYet.SetLODs(lods);
            }
        }

        AssetDatabase.StartAssetEditing();
        foreach(var p in allPrefInst)
        {
            var setYet = p.inst.GetComponent<LODGroup>();
            if (setYet)
            {
                PrefabUtility.ApplyPrefabInstance(p.inst, InteractionMode.AutomatedAction);
            }
        }

        AssetDatabase.StopAssetEditing();
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

    }

   
    public List<Mesh> allMesh;

    public List<string> pathes;

    public List<GameObject> allGo;
    public List<GameObject> allPrefabs;
    public List<PrefabInfo> allPrefInst;
    private Dictionary<string, bool> hasPrefab;
}
#endif
