﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugInputCenter : MonoBehaviour
{
    public InputController con;
    public GUITexture tex;
    public enum State
    {
        Idle,
        Move,
    }

    private State state = State.Idle;
    private Vector2 fingerPos = Vector2.zero;

    void Update()
    {
        if (state == State.Idle)
        {
            var pos = new Vector2(Screen.width - Screen.width / 7f, Screen.height / 4.5f);

            SetPos(pos);
        }
        else
        {
            SetPos(fingerPos);
        }
    }

    void SetPos(Vector2 p)
    {
        var rsz = this.con.GetFingerSize();
        tex.pixelInset = new Rect(p.x - rsz.x / 2, p.y - rsz.y / 2, rsz.x, rsz.y);
    }

    public void EnterMove()
    {
        state = State.Move;
    }

    public void ExitMove()
    {
        state = State.Idle;
    }

    public void SetFingerPos(Vector2 pos)
    {
        fingerPos = pos;
    }

    public Vector2 GetPos()
    {
        return fingerPos;
    }
}
