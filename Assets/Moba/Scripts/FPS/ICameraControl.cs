﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyLib;

/// <summary>
/// 镜头状态控制器
/// </summary>
public class ICameraControl
{
    public CameraController.State state;
    public virtual void OnEnter()
    {

    }
    public virtual void OnExit()
    {
    }

    public virtual void OnUpdate()
    {

    }
    public virtual void InitPlayer()
    {

    }
}
