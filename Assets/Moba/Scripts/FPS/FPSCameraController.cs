﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyLib;

public class FPSCamCon : ICameraControl
{
    private FPSCameraController fps;
    public FPSCamCon()
    {
        state = MyLib.CameraController.State.FPS;
    }
    public override void OnEnter()
    {
        fps = CameraController.cameraController.gameObject.AddMissingComponent<FPSCameraController>();
        fps.enabled = true;
        fps.SetCharDir = (qua) =>
        {
            var player = ObjectManager.Instance.GetMyPlayer();
            var phy = player.NCR(_=>_.GetComponent<IPhysicCom>());
            var dir = qua.eulerAngles.y;
            phy.NCC(_ => _.TurnToDir(dir)); 
        };

        //var go = new GameObject("_FPSInput");
        //go.AddMissingComponent<FPSInput>();
        GameObject.Instantiate(Resources.Load<GameObject>("levelPublic/_FPSInput"));
        ObjectProxyManager.Instance.HideMe();
    }

    public override void OnExit()
    {
        fps = CameraController.cameraController.gameObject.GetComponent<FPSCameraController>();
        fps.NCC(_=>_.enabled = false);
        if (FPSInput.Instance != null)
        {
            GameObject.Destroy(FPSInput.Instance.NCR(_ => _.gameObject));
        }
    }
    public override void InitPlayer()
    {
        fps.NCC(_ => _.InitPlayer());
    }
}
/// <summary>
/// 右侧负责镜头视角屏幕上滑动控制镜头角度
/// 左侧遥感负责移动
/// </summary>
public class FPSCameraController : MonoBehaviour
{
    public static FPSCameraController Instance;
    public GameObject player;
    public System.Action<Quaternion> SetCharDir;

    private enum State
    {
        Idle, 
        Trace,
    }
    private State state = State.Idle;

    private void Awake()
    {
        Instance = this;   
    }

    public Vector3 offsetPos = new Vector3(0, 3.3f, 0);
    public GameObject tracePos;
    private Quaternion mCharQua;
    private Quaternion mCamQua;

    public Quaternion mCurCharQua;
    public Quaternion mCurCamQua;

    /// <summary>
    /// 镜头只有上下
    /// 人控制左右
    /// 旋转逻辑
    /// </summary>
    void OnEnable()
    {
        InitPlayer();
    }

    public void InitPlayer()
    {
        state = State.Idle;
        player = CameraController.cameraController.player;
        Debug.Log("FPSCameraController:" + player);
        if (player != null)
        {
            tracePos = new GameObject("_TracePos");
            tracePos.transform.parent = player.transform;
            Util.InitGameObject(tracePos);
            tracePos.transform.localPosition = offsetPos;

            transform.position = tracePos.transform.position;
            transform.rotation = tracePos.transform.rotation;

            mCharQua = player.transform.localRotation;
            mCamQua = tracePos.transform.localRotation;

            //逐渐插值到目标值
            mCurCamQua = mCamQua;
            mCurCharQua = mCharQua;

            state = State.Trace;
        }
    }
    void OnDisable()
    {

    }
    void LateUpdate()
    {
        if(state == State.Trace)
        {
            MouseLookMethod();
            //镜头位置
            transform.position = tracePos.transform.position;
            transform.rotation = tracePos.transform.rotation;
        }
    }

    public float XSensitivity = 2;
    public float YSensitivity = 2;
    public bool clampVerticalRotation = true;
    public float MinimumX = -90F;
    public float MaximumX = 90F;
    public bool smooth = false;
    public float smoothTime = 5f;
    public bool lockCursor = true;

    private bool m_cursorIsLocked = true;

    //通过双摇杆控制 类似于坦克大战
    private void MouseLookMethod()
    {
        var vec2 = FPSInput.Instance.GetMouseAxis();
        var yRot = vec2.x * XSensitivity;
        var xRot = vec2.y * YSensitivity;

        //暂时通过鼠标输入
        //float yRot = Input.GetAxis("Mouse X") * XSensitivity;
        //float xRot = Input.GetAxis("Mouse Y") * YSensitivity;

        //角色旋转
        mCharQua *= Quaternion.Euler(0, yRot, 0);
        //镜头旋转
        mCamQua *= Quaternion.Euler(-xRot, 0, 0);

        //限制镜头上下范围
        if (clampVerticalRotation)
            mCamQua = ClampRotationAroundXAxis(mCamQua);

        if (smooth)
        {
            mCurCharQua = Quaternion.Slerp(mCurCharQua, mCharQua, smoothTime * Time.deltaTime);
            mCurCamQua = Quaternion.Slerp(mCurCamQua, mCamQua, smoothTime * Time.deltaTime);
        }
        else
        {
            mCurCharQua = mCharQua;
            mCurCamQua = mCamQua;
        }

        //设置角色转身
        SetCharDir.NCC(_ => _.Invoke(mCurCharQua));
        //设置上下仰角 纯客户端
        tracePos.transform.localRotation = mCurCamQua;
        //UpdateCursorLock();

    }

    Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

        angleX = Mathf.Clamp(angleX, MinimumX, MaximumX);

        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

        return q;
    }
    private void UpdateCursorLock()
    {
        //if the user set "lockCursor" we check & properly lock the cursos
        if (lockCursor)
            InternalLockUpdate();
    }
    private void InternalLockUpdate()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            m_cursorIsLocked = false;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            m_cursorIsLocked = true;
        }

        if (m_cursorIsLocked)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else if (!m_cursorIsLocked)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    /*
    private Vector2 initPos;
    private Vector2 updatePos;
    public void OnTouchStart(Vector2 curPos)
    {
        if(state == State.Idle)
        {
            state = State.TouchBegin;
            initPos = curPos;
            updatePos = curPos;
        }
    }

    public void OnTouchEnd()
    {
        if(state == State.TouchBegin)
        {
            state = State.Idle;
        }
    }
    /// <summary>
    /// 每帧的
    /// </summary>
    /// <param name="dir"></param>
    public void UpdateTouchPos(Vector2 newPos)
    {
        if(state == State.TouchBegin)
        {
            updatePos = newPos;
        }
    }

    public void Update()
    {
        if(state == State.TouchBegin)
        {

        }
    }
    */
}

