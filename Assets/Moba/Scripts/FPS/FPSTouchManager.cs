﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSTouchManager : MonoBehaviour
{
    public const int MouseFingerId = 999;

    public static FPSTouchManager Instance;
    void Awake()
    {
        Instance = this;
    }

    private Dictionary<int, bool> fingers = new Dictionary<int, bool>();
    public bool CheckInUse(int fingerId)
    {
        //已经点击UI了
        if(UICamera.hoveredObject != null)
        {
            return true;
        }
        return fingers.ContainsKey(fingerId);
    }
    public void UseFinger(int fingerId)
    {
        fingers[fingerId] = true;
    }
    public void ClearFinger(int fingerId)
    {
        //fingers[fingerId] = false;
        fingers.Remove(fingerId);
    }
    public void Clear()
    {
        fingers.Clear();
    }
}
