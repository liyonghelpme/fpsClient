﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 镜头控制输入检测
/// FPS镜头初始化的时候 该输入设备初始化
/// </summary>
public class FPSInput : MonoBehaviour, InputController
{
    //监控FPS 鼠标中心点--》边缘 用于Debug
    public Color activeColor;
    public Color inactiveColor;

    public Texture2D joyStick;
    public Texture2D background2D;

    public float joyWidth = 350;
    public float joyHeight = 350;
    public Vector2 fingerSize = new Vector2(100, 100);

    private float size;
    private GUITexture joyStickTexture;
    private GUITexture backObj;

    public float InnerRadius = 80;
    public float ExternalRadius = 130;
    public float CancelRadius = 50;

    private DebugInputBack rb;
    private DebugInputCenter rf;

    private enum TouchState
    {
        Idle,
        Move,
    }
    private TouchState state = TouchState.Idle;
    private FPSCameraController fpsCameraController;
    public static FPSInput Instance;
    void Awake()
    {
        Instance = this;
        joyStickTexture = gameObject.AddComponent<GUITexture>();
        joyStickTexture.texture = joyStick;
        joyStickTexture.color = inactiveColor;

        var bo = new GameObject("RightBack");
        bo.transform.parent = transform;
        bo.transform.localScale = Vector3.zero;
        backObj = bo.AddComponent<GUITexture>();
        backObj.texture = background2D;
        backObj.color = inactiveColor;

        rb = gameObject.AddComponent<DebugInputBack>();
        rb.con = this;
        rb.tex = backObj;

        rf = gameObject.AddComponent<DebugInputCenter>();
        rf.con = this;
        rf.tex = joyStickTexture;

        MyLib.Util.InitGameObject(gameObject);
        transform.localScale = Vector3.zero;
    }

    public enum InputMode
    {
        Mouse,
        Touch,
    }
    public InputMode inputMode = InputMode.Touch;
    //也检测鼠标作为一个Touch输入源 在手机上的时候
    public bool checkMouseTouch = true;

    void OnEnable()
    {
        state = TouchState.Idle;
    }

    private Vector2 fingerInitPos;
    private int fingerId = -1;
    private void SetInitFinger(Vector2 fp)
    {
        fingerInitPos = fp;
    }

    //1024 * 768 200 旋转到边缘
    public float touchRadius = 250;
    public float deadZone = 30;

    private Vector2 GetTouchDir(Vector2 newPos)
    {
        var deltaPos = newPos - fingerInitPos;
        var dx = deltaPos.x;
        var dy = deltaPos.y;
        var rx = Mathf.Clamp01((Mathf.Abs(dx) - deadZone) / (touchRadius - deadZone));
        var ry = Mathf.Clamp01((Mathf.Abs(dy) - deadZone) / (touchRadius - deadZone));

        //Touch 应该使用 DeadZone 避免过于敏感
        //var mag = deltaPos.magnitude;
        //var rate = Mathf.Clamp01((mag - deadZone) / (touchRadius-deadZone));

        var sx = Mathf.Sign(deltaPos.x);
        var sy = Mathf.Sign(deltaPos.y);

        //越大 Axis X Y的运动速度 越大？
        return new Vector2(sx * rx, sy * ry);
    }
    private void OnDestroy()
    {
        FPSTouchManager.Instance.Clear();
    }
    /// <summary>
    /// 鼠标模式或者Touch模式
    /// </summary>
    /// <returns></returns>
    public Vector2 GetMouseAxis()
    {
        if (inputMode == InputMode.Mouse)
        {
            var x = Input.GetAxis("Mouse X");
            var y = Input.GetAxis("Mouse Y");
            return new Vector2(x, y);
        }

        //Touch 操作
        Vector2 curPos = Vector2.zero;
        if(state == TouchState.Idle)
        {
            //获取一个Touch 没有被Left捕获的
            foreach (var touch in Input.touches)
            {
                fingerId = touch.fingerId;
                var inUse = FPSTouchManager.Instance.CheckInUse(fingerId);
                //初次接触屏幕
                if (touch.phase == TouchPhase.Began && !inUse && touch.position.x > Screen.width / 2)
                {
                    //确定有效性 没有被左侧捕获
                    //if (Input.GetTouch(fingerId).position.x > Screen.width / 2)
                    {
                        state = TouchState.Move;
                        var fp = Input.GetTouch(fingerId).position;
                        SetInitFinger(fp);
                        FPSTouchManager.Instance.UseFinger(fingerId);
                        rb.EnterMove();
                        rf.EnterMove();
                        rb.SetFingerPos(fp);
                        rf.SetFingerPos(fp);
                        break;
                    }
                }
            }
            if(state == TouchState.Idle && checkMouseTouch)
            {
                fingerId = FPSTouchManager.MouseFingerId;
                var inUse = FPSTouchManager.Instance.CheckInUse(fingerId);
                var mousePos = Input.mousePosition;
                if (!inUse && Input.GetMouseButton(0) && mousePos.x > Screen.width/2) //鼠标左键
                {
                    state = TouchState.Move;
                    var fp = Input.mousePosition;
                    SetInitFinger(Input.mousePosition);
                    FPSTouchManager.Instance.UseFinger(fingerId);

                    rb.EnterMove();
                    rf.EnterMove();
                    rb.SetFingerPos(fp);
                    rf.SetFingerPos(fp);
                }
            }
        }else if(state == TouchState.Move)
        {
            var curFingerPos = Vector2.zero;
            var getTouch = false;
            var finishTouch = false;
            var isMouse = false;
            Touch touch = new Touch();
            foreach(var t in Input.touches)
            {
                if(t.fingerId == fingerId)
                {
                    touch = t;
                    curFingerPos = t.position;
                    getTouch = true;
                    break;
                }
            }
            if(checkMouseTouch)
            {
                if(fingerId == FPSTouchManager.MouseFingerId)
                {
                    curFingerPos = Input.mousePosition;
                    getTouch = true;
                    isMouse = true;
                }
            }

            if (getTouch)
            {
                if (isMouse)
                {
                    finishTouch = !Input.GetMouseButton(0);
                }
                else
                {
                    var phase = touch.phase;
                    finishTouch = phase == TouchPhase.Ended || phase == TouchPhase.Canceled;
                }
            }else
            {
                finishTouch = true;
            }

            if (finishTouch)
            {
                state = TouchState.Idle;
                FPSTouchManager.Instance.ClearFinger(fingerId);
                fingerId = -1;
                rb.ExitMove();
                rf.ExitMove();
            }else
            {
                var pos = curFingerPos;
                curPos = GetTouchDir(pos);
                rb.SetFingerPos(pos);
                rf.SetFingerPos(pos);
            }
        }
        return curPos;
    }

    public Vector2 GetRealSize()
    {
        var rate = Mathf.Min(Screen.width / 1024.0f, Screen.height / 768.0f);
        var v = new Vector2(joyWidth, joyHeight) * rate;
        return v;
    }

    public float GetInnerRadius()
    {
        return InnerRadius;
    }

    public float GetExternalRadius()
    {
        return ExternalRadius;
    }

    public float GetCancelRadius()
    {
        return CancelRadius;
    }

    public void CancelShoot(bool cancelShoot)
    {
    }

    public Vector2 GetFingerSize()
    {
        var rate = Mathf.Min(Screen.width / 1024.0f, Screen.height / 768.0f);
        var v = fingerSize * rate;
        return v;
    }

    public bool IsDebug()
    {
        return true;
    }
}
