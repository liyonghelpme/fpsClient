﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyLib;

public static class RepUtil
{
    public static void NCC<T>(this T obj, System.Action<T> func)
    {
        if(obj != null)
        {
            func(obj);
        }
    }
    public static R NCR<T, R>(this T obj, System.Func<T, R> func)
    {
        if (obj != null)
        {
            return func(obj);
        }
        return default(R);
    }

    public static void WaitCall(System.Func<bool> con, System.Action func)
    {
        ObjectManager.Instance.StartCoroutine(DoFunc(con, func));
    }

    private static IEnumerator DoFunc(System.Func<bool> con, System.Action func)
    {
        while (!con())
        {
            yield return null;
        }
        func();
    }
}
