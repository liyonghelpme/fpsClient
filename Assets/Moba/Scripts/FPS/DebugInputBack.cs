﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public interface InputController
{
    Vector2 GetRealSize();
    float GetInnerRadius();
    float GetExternalRadius();
    float GetCancelRadius();

    void CancelShoot(bool cancelShoot);

    Vector2 GetFingerSize();
    bool IsDebug();
}

/// <summary>
/// 用于追踪点击位置中心
/// </summary>
public class DebugInputBack : MonoBehaviour
{
    public enum State
    {
        Idle,
        Move,
    }

    public GUITexture tex;
    public InputController con;
    private State state = State.Idle;

    public float screenMoveWidthRatio = 0.8f;
    public float screenMoveHeightRatio = 0.8f;

    void Update()
    {
        if (state == State.Idle)
        {
            var pos = new Vector2(Screen.width - Screen.width / 7f, Screen.height / 4.5f);
            SetPos(pos);
        }
    }

    void SetPos(Vector2 p)
    {
        if (p.x - Screen.width * 0.5f * (1 + screenMoveWidthRatio) > 0)
        {
            p.x = Screen.width * 0.5f * (1 + screenMoveWidthRatio);
        }
        else if (p.x - Screen.width * 0.5f * (1 - screenMoveWidthRatio) < 0)
        {
            p.x = Screen.width * 0.5f * (1 - screenMoveWidthRatio);
        }

        var rsz = this.con.GetRealSize();

        if (p.y - Screen.height * 0.5f * (1 + screenMoveHeightRatio) + rsz.y / 2f > 0)
        {
            p.y = Screen.height * 0.5f * (1 + screenMoveHeightRatio) - rsz.y / 2f;

        }
        else if (p.y - Screen.height * 0.5f * (1 - screenMoveHeightRatio) - rsz.y / 2f < 0)
        {
            p.y = Screen.height * 0.5f * (1 - screenMoveHeightRatio) + rsz.y / 2f;
        }

        tex.pixelInset = new Rect(p.x - rsz.x / 2, p.y - rsz.y / 2, rsz.x, rsz.y);
    }

    private bool first = false;
    private Vector2 lastPos = Vector2.zero;
    private Vector2 initPos = Vector2.zero;

    public void EnterMove()
    {
        state = State.Move;
        first = true;
    }

    public void ExitMove()
    {
        state = State.Idle;
    }

    private bool isCancel = true;

    public bool IsCancel()
    {
        return isCancel;
    }

    public Vector2 GetPos()
    {
        return initPos;
    }

    public void SetFingerPos(Vector2 pos)
    {
        if (first)
        {
            first = false;
            lastPos = pos;
            initPos = pos;
            isCancel = true;
            SetPos(pos);
        }
        else
        {
            /*
            //根据手指位置调整
            var diff = pos - initPos;
            var dir = pos - lastPos;
            lastPos = pos;

            //手指在中心半径范围内 50半径 相反运动
            var mag = diff.magnitude;
            var iner = this.con.GetInnerRadius() * RightController.GetRate();
            var external = this.con.GetExternalRadius() * RightController.GetRate();
            if (mag < iner)
            {
                initPos -= dir;
                SetPos(initPos);

                //手指在圆环外面 跟随手指移动
            }
            else if (mag > external)
            {
                var fingerDir = pos - initPos;
                var distOff = fingerDir.normalized * external;
                initPos = pos - distOff;
                //initPos += dir;
                SetPos(initPos);
            }
            if (mag < this.con.GetCancelRadius() * RightController.GetRate())
            {
                isCancel = true;
                con.CancelShoot(isCancel);
            }
            else
            {
                isCancel = false;
                con.CancelShoot(isCancel);
            }
            */
        }
    }

}
