﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyLib;

/// <summary>
/// 设置对象状态
/// </summary>
public class ObjectProxyManager : MonoBehaviour
{
    public static ObjectProxyManager Instance;
    private void Awake()
    {
        Instance = this;
    }

    public void HideMe()
    {
        RepUtil.WaitCall(() =>
        {
            return ObjectManager.Instance.myPlayerGameObj != null;
        },()=>
        {
            var attr = ObjectManager.Instance.myPlayerGameObj.GetComponent<NpcAttribute>();
            attr.SetVisible(false);
        });
    }
}
