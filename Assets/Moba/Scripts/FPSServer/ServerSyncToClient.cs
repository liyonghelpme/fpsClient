﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyLib;

/// <summary>
/// 服务器上角色自己从服务器逻辑上获得表现数据
/// </summary>
public class ServerSyncToClient : MonoBehaviour
{
    public AvatarInfo curInfo;
    void Awake()
    {
        curInfo = AvatarInfo.CreateBuilder().result;
    }
    
    public void CheckPlayerAvatarInfo()
    {
        var attribute = GetComponent<AIBase>().GetAI().attribute;
        var info = attribute.fpsPlayer.avatarInfo;
        Debug.Log("CheckPlayerAvatarInfo:"+info);
        if (info.HasPlayerModelInGame && curInfo.PlayerModelInGame != info.PlayerModelInGame)
        {
            curInfo.PlayerModelInGame = info.PlayerModelInGame;
            StartCoroutine(GetComponent<MobaModelLoader>().LoadModel(curInfo.PlayerModelInGame));
            var unitData = Util.GetUnitData(true, curInfo.PlayerModelInGame, 0);
            attribute.SetObjUnitData(unitData);
        }
    }
}
