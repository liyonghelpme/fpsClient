﻿using MyLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;
using Google.ProtocolBuffers;
using KBEngine;
using System.Threading.Tasks;
using System.Collections.Concurrent;

/// <summary>
/// Unity客户端服务器状态
/// 每次Close之后重新构建新的状态
/// </summary>
public class FPSTcpConnect 
{
    /// <summary>
    /// 是否需要鉴权
    /// </summary>
    public bool needAuth = false;
    public string authToken = null;
    public Func<FPSTcpConnect, bool> CheckToken;

    //逻辑处理
    public System.Action closeHandler;
    public System.Action<KBEngine.Packet> handler;
    //管理器
    public System.Action OnNetClose;

    byte[] mTemp = new byte[8192];
    MessageReader msgReader = new MessageReader();

    private TcpClient sock;
    NetworkStream dualStream;
    private FPSTcpServer serv;
    private IMainLoop ml;
    public enum State
    {
        NotAuth,
        Idle,
        Connected,
        Close,
    }
    public ConcurrentDictionary<byte, MessageHandler> flowHandler = new ConcurrentDictionary<byte, MessageHandler>(); 
    /// <summary>
    /// 逻辑状态
    /// </summary>
    public State state = State.NotAuth;

    public FPSTcpConnect(FPSTcpServer server, TcpClient socket)
    {
        ml = server.mainLoop;
        msgReader.mainLoop = ml;
        msgReader.msgHandle = HandleMsg;

        state = State.Connected;
        sock = socket;
        serv = server;
        dualStream = sock.GetStream();
    }

    private void HandleMsg(KBEngine.Packet packet)
    {
        Debug.Log("FPSTcpConnect:HandleMsg:"+packet.protoBody);
        if (state == State.NotAuth)
        {
            var shake = packet.protoBody as MMHandShake;
            authToken = shake.Token;
            var ok = CheckToken(this);
            var ret = MMHandShake.CreateBuilder();
            ret.Success = ok;
            Debug.Log("HandShakeSuc:"+ret);
            //正常接收报文
            if (ok)
            {
                state = State.Connected;
            }

            SendPacket(ret, packet.flowId);
            Debug.Log("FPSTcp:HandShake:"+shake+":"+ret);
        }
        else
        {
            if (handler != null) handler(packet);
        }
    }

    /// <summary>
    /// 等Connect初始化完成才开始接收报文
    /// </summary>
    public void StartReceive()
    {
        GetPackets();
    }

    private async Task GetPackets()
    {
        while (sock != null && sock.Connected && (state == State.Connected || state == State.NotAuth))
        {
            try
            {
                await GetOnePacket();
            }
            catch (Exception exp)
            {
                Log.Net("StartReceive:" + exp.ToString());
                Close();
            }
        }
    }
    private async Task GetOnePacket()
    {
        await Task.Factory.FromAsync(BeginGetPacket, EndGetPacket, null);
    }
    private IAsyncResult BeginGetPacket(AsyncCallback callback, object state)
    {
        var result = dualStream.BeginRead(mTemp, 0, mTemp.Length, callback, state);
        return result;
    }

    /// <summary>
    /// 执行线程不确定
    /// </summary>
    /// <param name="result"></param>
    private void EndGetPacket(IAsyncResult result)
    {
        var bytes = 0;
        try
        {
            bytes = dualStream.EndRead(result);
        }
        catch (Exception exp)
        {
            Debug.Log("EndGetPack:" + exp.ToString());
            Close();
            return;
        }

        if (bytes <= 0)
        {
            Debug.Log("Connect Lost");
            Close();
            return;
        }
        msgReader.process(mTemp, (uint)bytes, flowHandler);
    }


    /// <summary>
    /// 线程不安全函数
    /// </summary>
    private void Close()
    {
        if (state == State.Close) return;
        ml.queueInLoop(() =>
        {
            MainThreadClose();
        });
    }

    private void MainThreadClose()
    {
        if (state == State.Close) return;
        state = State.Close;
        try
        {
            if (sock != null && sock.Connected)
            {
                //sock.Shutdown(SocketShutdown.Both);
                sock.Close();
            }
        }catch(Exception exp)
        {
            Debug.LogError("Close Except:"+exp.ToString());
        }
        sock = null;
        if (OnNetClose !=null ) OnNetClose();
        if(closeHandler != null)
        {
            closeHandler();
        }
    }

    public void SendPacket(IBuilderLite cmd, byte ackId=0, byte flowId = 0)
    {
        if (state == State.Close) return;
        Bundle.GetPacketAckFlow(cmd, ackId, flowId, out Bundle bundle);
        msgBuffers.Add(bundle);
        if(sendState == SendState.Idle)
        {
            Consume();
        }
    }

    private enum SendState
    {
        Idle, 
        InSend,
    }
    private SendState sendState = SendState.Idle;
    private List<Bundle> msgBuffers = new List<Bundle>();

    private Bundle curBundle;
    private async Task Consume()
    {
        while(msgBuffers.Count > 0 && state != State.Close)
        {
            Bundle toSend = null;
            toSend = msgBuffers[0];
            sendState = SendState.InSend;
            msgBuffers.RemoveAt(0);
            curBundle = toSend;
            try
            {
                await Task.Factory.FromAsync(BeginSendPacket, EndSendPacket, null);
            }catch(Exception exp)
            {
                Debug.LogError("Consume:"+exp.ToString());
                Close();
            }
            Bundle.ReturnBundle(curBundle);
            curBundle = null;
        }

        sendState = SendState.Idle;
    }

    private IAsyncResult BeginSendPacket(AsyncCallback callback, object state)
    {
        return dualStream.BeginWrite(curBundle.coutStream.GetBuffer(), 0, curBundle.coutStream.position, callback, state);
    }
    private void EndSendPacket(IAsyncResult result)
    {
        dualStream.EndWrite(result);
    }
}
