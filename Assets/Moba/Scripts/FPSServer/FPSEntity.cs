﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyLib;

/// <summary>
/// 服务器上Npc数据
/// </summary>
public class FPSEntity 
{
    public EntityInfo lastEntityInfo;
    public EntityInfo entityInfo;
    public EntityInfo GetEntityInfo()
    {
        return entityInfo;
    }

    public EntityInfo GetEntityInfoDiff()
    {
        var na1 = EntityInfo.CreateBuilder();
        na1.Id = entityInfo.Id;
        if (lastEntityInfo.X != entityInfo.X
            || lastEntityInfo.Y != entityInfo.Y || lastEntityInfo.Z != entityInfo.Z
            || lastEntityInfo.SpeedX != entityInfo.SpeedX
            || lastEntityInfo.SpeedY != entityInfo.SpeedY
            )
        {
            na1.X = entityInfo.X;
            na1.Y = entityInfo.Y;
            na1.Z = entityInfo.Z;
            na1.SpeedX = entityInfo.SpeedX;
            na1.SpeedY = entityInfo.SpeedY;

            na1.Changed = true;

            lastEntityInfo.X = entityInfo.X;
            lastEntityInfo.Y = entityInfo.Y;
            lastEntityInfo.Z = entityInfo.Z;
            lastEntityInfo.SpeedX = entityInfo.SpeedX;
            lastEntityInfo.SpeedY = entityInfo.SpeedY;
        }

        if (entityInfo.UnitId != lastEntityInfo.UnitId)
        {
            na1.UnitId = entityInfo.UnitId;
            na1.Changed = true;

            lastEntityInfo.UnitId = entityInfo.UnitId;
        }

        if (entityInfo.SpawnId != lastEntityInfo.SpawnId)
        {
            na1.SpawnId = entityInfo.SpawnId;
            na1.Changed = true;

            lastEntityInfo.SpawnId = entityInfo.SpawnId;
        }
        if (entityInfo.HP != lastEntityInfo.HP)
        {
            na1.HP = entityInfo.HP;
            na1.Changed = true;

            lastEntityInfo.HP = entityInfo.HP;
        }

        if (entityInfo.TeamColor != lastEntityInfo.TeamColor)
        {
            na1.TeamColor = entityInfo.TeamColor;
            na1.Changed = true;
            lastEntityInfo.TeamColor = entityInfo.TeamColor;
        }

        if (entityInfo.Dir != lastEntityInfo.Dir)
        {
            na1.Dir = entityInfo.Dir;
            na1.Changed = true;
            lastEntityInfo.Dir = entityInfo.Dir;
        }

        //直接field 拷贝 不用 复制整个了
        return na1.Build();
    }

}

