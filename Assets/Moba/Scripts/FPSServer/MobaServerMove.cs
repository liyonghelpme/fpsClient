﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyLib;

public class MobaServerMove : MoveState
{
    IPhysicCom physics;
    FPSPlayer me;
    public override void Init()
    {
        me = GetPlayer();
        physics = GetAttr().GetComponent<IPhysicCom>();
    }

    public override void EnterState()
    {
        base.EnterState();
        aiCharacter.SetRun();
    }

    /// <summary>
    /// 客户端物理移动通过FixedUpdate来处理
    /// </summary>
    /// <returns></returns>
    public override IEnumerator RunLogic()
    {
        var fixedWait = new WaitForFixedUpdate();

        var tempNum = runNum;
        while(CheckInState(tempNum))
        {
            var clientPos = me.GetClientVelocity();
            if(FPSServerUtil.IsClientMove(clientPos))
            {
                MoveSmooth();
                yield return fixedWait;
            }
            else
            {
                break;
            }
        }
        if (CheckInState(tempNum))
        {
            aiCharacter.ChangeState(AIStateEnum.IDLE);
        }
    }

    public override void ExitState()
    {
        var avatarInfo = GetPlayer().avatarInfo;
        avatarInfo.SpeedX = 0;
        avatarInfo.SpeedY = 0;
        base.ExitState();
    }
    private void MoveSmooth()
    {
        var speed = GetAttr().GetSpeed();
        var clientDir = me.GetClientVelocity();
        clientDir.Normalize();
        var speedDir = speed * clientDir;

        var avatarInfo = me.GetAvatarInfo();
        avatarInfo.SpeedX = FPSServerUtil.RealToNetPos(speedDir.x);
        avatarInfo.SpeedY = FPSServerUtil.RealToNetPos(speedDir.y);

        var deltaTime = Time.fixedDeltaTime;
        var curPos = me.GetFloatPos();
        var newPos = curPos + deltaTime * speedDir;
        var retPos = physics.MoveToWithPhysic(newPos);

        GetPlayer().SetPos(retPos);
    }
}
