﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyLib;



/// <summary>
/// 同步客户端
/// 服务器物理
/// </summary>
[RequireComponent(typeof(ServerSyncToClient))]
[RequireComponent(typeof(MobaMePhysic))]
[RequireComponent(typeof(MobaModelLoader))]
public class MobaServerPlayer : AIBase
{
    void Awake()
    {
        Debug.Log("CreateMobaServerPlayer:");
        attribute = GetComponent<NpcAttribute>();
        ai = new MobaPlayerCharacter();
        ai.attribute = attribute;
        ai.AddState(new MobaServerIdle());
        ai.AddState(new MobaServerMove());
        ai.AddState(new MobaServerSkill());
        ai.AddState(new MobaServerDead());
    }
    private void Start()
    {
        InitAvatarInfo();
        ai.ChangeState(AIStateEnum.IDLE);
    }

    /// <summary>
    /// 变动时同步
    /// 类似于OtherSync
    /// </summary>
    private void InitAvatarInfo()
    {
        GetComponent<ServerSyncToClient>().CheckPlayerAvatarInfo();
    }
}
