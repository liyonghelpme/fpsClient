﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyLib;
using System;
using MHType = System.Action<KBEngine.Packet>;
using KBEngine;
using System.Reflection;

/// <summary>
/// 服务器上控制FPS玩家
/// --->GameObject ---> Player
/// 独立存储在
/// </summary>
public class FPSPlayer
{
    /// <summary>
    /// 客户端连接状态
    /// </summary>
    public enum State
    {
        NotAuth,
        Auth,
        ReadyClientMap,
    }
    public State state = State.NotAuth;
    public NpcAttribute npcObj;

    public AvatarInfo lastAvatarInfo;
    public AvatarInfo avatarInfo;
    public FPSTcpConnect connect;
    private Dictionary<string, MHType> handlers = new Dictionary<string, MHType>();

    public FPSPlayer()
    {
        var methods = this.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
        foreach(var m in methods)
        {
            var attris = m.GetCustomAttributes(typeof(MsgHandleMethod), false);
            if(attris.Length > 0)
            {
                var action = (MHType)Delegate.CreateDelegate(typeof(MHType), this, m);
                handlers.Add(m.Name, action);
            }
        }
    }

    /// <summary>
    /// 初始化LastAvatarInfo
    /// </summary>
    public void Init()
    {
        lastAvatarInfo = AvatarInfo.CreateBuilder(avatarInfo).Build();
        avatarInfo.Level = 1;
        avatarInfo.Gold = 30;
        avatarInfo.Exp = 0;
    }

    public void SetConnect(FPSTcpConnect con)
    {
        connect = con;
        connect.handler = MsgHandle;
        connect.closeHandler = OnClose;
        state = State.Auth;
    }

    /// <summary>
    /// 处理客户端发送的FPS游戏网络报文
    /// </summary>
    /// <param name="p"></param>
    private void MsgHandle(KBEngine.Packet p)
    {
        Debug.Log("FPSPLayer:MsgHandle:"+p.protoBody);
        var cmd = p.protoBody as CGPlayerCmd;
        var cmds = cmd.Cmd.Split(' ');
        var cmd0 = cmds[0];
        if (handlers.ContainsKey(cmd0))
        {
            handlers[cmd0](p);
        }
    }

    [MsgHandleMethod]
    private void InitData(KBEngine.Packet p)
    {
        var gc = GCPlayerCmd.CreateBuilder();
        gc.Result = "InitData";
        //proxy.SendPacket(gc, msg.packet.flowId, 0);
        connect.SendPacket(gc, p.flowId); 
    }

    /// <summary>
    /// 客户端发送命令
    /// </summary>
    /// <param name="p"></param>
    [MsgHandleMethod]
    private void UpdateData(Packet p)
    {
        var cmd = p.protoBody as CGPlayerCmd;
        if (cmd.AvatarInfo.HasIsRobot) avatarInfo.IsRobot = cmd.AvatarInfo.IsRobot;
        SetClientSyncPos(cmd);
    }

    [MsgHandleMethod]
    private void Ready(KBEngine.Packet p)
    {
        state = State.ReadyClientMap;
        avatarInfo.State = PlayerState.AfterReset;
    }


    private void OnClose()
    {

    }

    /// <summary>
    /// 同步客户端移动指令
    /// </summary>
    /// <param name="cmd"></param>
    private void SetClientSyncPos(CGPlayerCmd cmd)
    {
        if (avatarInfo.ResetPos) return;//出生重置位置
        if (avatarInfo.State != PlayerState.AfterReset) return;
        var info = cmd.AvatarInfo;
        if(info.HasX)
        {
            info.FrameID = cmd.FrameId;
            positions.Add(info);
            if(positions.Count > 5)
            {
                positions.RemoveAt(0);
            }
        }
    }

    /// <summary>
    /// 获取客户端移动速度
    /// </summary>
    /// <returns></returns>
    public Vector3 GetClientVelocity()
    {
        if(positions.Count > 0)
        {
            var p1 = positions[positions.Count - 1];
            var speed = new Vector3(p1.SpeedX, 0, p1.SpeedY);
            return speed;
        }
        return Vector3.zero;
    }

    private List<AvatarInfo> positions = new List<AvatarInfo>();


    public AvatarInfo GetAvatarInfo()
    {
        return avatarInfo;
    }
    public AvatarInfo.Builder GetAvatarInfoDiff()
    {
        var na1 = AvatarInfo.CreateBuilder();
        na1.Id = avatarInfo.Id;
        if (avatarInfo.HP != lastAvatarInfo.HP)
        {
            na1.HP = avatarInfo.HP;
            na1.Changed = true;

            lastAvatarInfo.HP = avatarInfo.HP;
        }
        if (avatarInfo.TeamColor != lastAvatarInfo.TeamColor)
        {
            na1.TeamColor = avatarInfo.TeamColor;
            na1.Changed = true;

            lastAvatarInfo.TeamColor = avatarInfo.TeamColor;
        }
        if (avatarInfo.IsMaster != lastAvatarInfo.IsMaster)
        {
            na1.IsMaster = avatarInfo.IsMaster;
            na1.Changed = true;

            lastAvatarInfo.IsMaster = avatarInfo.IsMaster;
        }
        if (avatarInfo.NetSpeed != lastAvatarInfo.NetSpeed)
        {
            na1.NetSpeed = avatarInfo.NetSpeed;
            na1.Changed = true;

            lastAvatarInfo.NetSpeed = avatarInfo.NetSpeed;
        }

        if (avatarInfo.ThrowSpeed != lastAvatarInfo.ThrowSpeed)
        {
            na1.ThrowSpeed = avatarInfo.ThrowSpeed;
            na1.Changed = true;

            lastAvatarInfo.ThrowSpeed = avatarInfo.ThrowSpeed;
        }

        if (avatarInfo.JumpForwardSpeed != lastAvatarInfo.JumpForwardSpeed)
        {
            na1.JumpForwardSpeed = avatarInfo.JumpForwardSpeed;
            na1.Changed = true;

            lastAvatarInfo.JumpForwardSpeed = avatarInfo.JumpForwardSpeed;
        }
        if (avatarInfo.Name != lastAvatarInfo.Name)
        {
            na1.Name = avatarInfo.Name;
            na1.Changed = true;

            lastAvatarInfo.Name = avatarInfo.Name;
        }

        if (avatarInfo.Job != lastAvatarInfo.Job)
        {
            na1.Job = avatarInfo.Job;
            na1.Changed = true;

            lastAvatarInfo.Job = avatarInfo.Job;
        }

        if (avatarInfo.KillCount != lastAvatarInfo.KillCount)
        {
            na1.KillCount = avatarInfo.KillCount;
            na1.Changed = true;

            lastAvatarInfo.KillCount = avatarInfo.KillCount;
        }

        if (avatarInfo.DeadCount != lastAvatarInfo.DeadCount)
        {
            na1.DeadCount = avatarInfo.DeadCount;
            na1.Changed = true;

            lastAvatarInfo.DeadCount = avatarInfo.DeadCount;
        }

        if (avatarInfo.SecondaryAttackCount != lastAvatarInfo.SecondaryAttackCount)
        {
            na1.SecondaryAttackCount = avatarInfo.SecondaryAttackCount;
            na1.Changed = true;

            lastAvatarInfo.SecondaryAttackCount = avatarInfo.SecondaryAttackCount;
        }

        if (avatarInfo.Score != lastAvatarInfo.Score)
        {
            na1.Score = avatarInfo.Score;
            na1.Changed = true;

            lastAvatarInfo.Score = avatarInfo.Score;
        }

        if (avatarInfo.ContinueKilled != lastAvatarInfo.ContinueKilled)
        {
            na1.ContinueKilled = avatarInfo.ContinueKilled;
            na1.Changed = true;

            lastAvatarInfo.ContinueKilled = avatarInfo.ContinueKilled;
        }

        if (avatarInfo.PlayerModelInGame != lastAvatarInfo.PlayerModelInGame)
        {
            na1.PlayerModelInGame = avatarInfo.PlayerModelInGame;
            na1.Changed = true;
            lastAvatarInfo.PlayerModelInGame = avatarInfo.PlayerModelInGame;
        }

        if (avatarInfo.Level != lastAvatarInfo.Level)
        {
            na1.Level = avatarInfo.Level;
            na1.Changed = true;
            lastAvatarInfo.Level = avatarInfo.Level;
        }
        if (avatarInfo.Exp != lastAvatarInfo.Exp)
        {
            na1.Exp = avatarInfo.Exp;
            na1.Changed = true;
            lastAvatarInfo.Exp = avatarInfo.Exp;
        }
        if (avatarInfo.State != lastAvatarInfo.State)
        {
            na1.State = avatarInfo.State;
            na1.Changed = true;
            lastAvatarInfo.State = avatarInfo.State;
        }
        if (avatarInfo.Gold != lastAvatarInfo.Gold)
        {
            na1.Gold = avatarInfo.Gold;
            na1.Changed = true;
            lastAvatarInfo.Gold = avatarInfo.Gold;
        }

        if (avatarInfo.ItemInfoList.dirty)
        {
            na1.ItemInfoDirty = true;
            na1.result.ItemInfoList = avatarInfo.ItemInfoList;
            na1.Changed = true;
            lastAvatarInfo.ItemInfoList = avatarInfo.ItemInfoList;
            avatarInfo.ItemInfoList.ClearDirty();
        }
        return na1;
    }

    public AvatarInfo.Builder GetPosInfoDiff()
    {
        var na1 = AvatarInfo.CreateBuilder();
        na1.Id = avatarInfo.Id;

        if (avatarInfo.X != lastAvatarInfo.X
            || avatarInfo.Y != lastAvatarInfo.Y
            || avatarInfo.Z != lastAvatarInfo.Z
            || avatarInfo.Dir != lastAvatarInfo.Dir
            || avatarInfo.SpeedX != lastAvatarInfo.SpeedX
            || avatarInfo.SpeedY != lastAvatarInfo.SpeedY)
        {
            na1.X = avatarInfo.X;
            na1.Y = avatarInfo.Y;
            na1.Z = avatarInfo.Z;
            na1.Dir = avatarInfo.Dir;
            na1.SpeedX = avatarInfo.SpeedX;
            na1.SpeedY = avatarInfo.SpeedY;
            na1.Changed = true;

            lastAvatarInfo.X = avatarInfo.X;
            lastAvatarInfo.Y = avatarInfo.Y;
            lastAvatarInfo.Z = avatarInfo.Z;
            lastAvatarInfo.Dir = avatarInfo.Dir;
            lastAvatarInfo.SpeedX = avatarInfo.SpeedX;
            lastAvatarInfo.SpeedY = avatarInfo.SpeedY;
        }

        if (avatarInfo.ResetPos)
        {
            na1.ResetPos = true;
            na1.Changed = true;
            avatarInfo.ResetPos = false;
        }


        if (avatarInfo.TowerDir != lastAvatarInfo.TowerDir)
        {
            na1.TowerDir = avatarInfo.TowerDir;
            na1.Changed = true;

            lastAvatarInfo.TowerDir = avatarInfo.TowerDir;
        }

        if (avatarInfo.FrameID != lastAvatarInfo.FrameID)
        {
            na1.FrameID = avatarInfo.FrameID;
            na1.Changed = true;
            lastAvatarInfo.FrameID = avatarInfo.FrameID;
        }
        return na1;
    }

    public Vector3 GetFloatPos()
    {
        var my = new MyVec3(avatarInfo.X, avatarInfo.Y, avatarInfo.Z);
        return my.ToFloat();
    }

    /// <summary>
    /// 设置位置之后同步给客户端
    /// </summary>
    /// <param name="p"></param>
    public void SetPos(Vector3 p1)
    {
        var p = MyVec3.FromVec3(p1);
        //var curPos = GetFloatPos();
        avatarInfo.X = p.x;
        avatarInfo.Y = p.y;
        avatarInfo.Z = p.z;
        //var newPos = GetFloatPos();
    }

    public void SendCmd(Google.ProtocolBuffers.IBuilderLite cmd, byte ackId = 0, byte flowId = 0)
    {
        connect.SendPacket(cmd, ackId, flowId);
    }
}
