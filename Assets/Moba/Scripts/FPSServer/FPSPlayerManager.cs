﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyLib;
using Google.ProtocolBuffers;

public class FPSPlayerManager : MonoBehaviour
{
    //新加入的玩家
    public List<AvatarInfo> newPlayer = new List<AvatarInfo>();
    public List<FPSPlayer> players = new List<FPSPlayer>();
    private List<AvatarInfo> removePlayer = new List<AvatarInfo>();

    private FPSServer fpsServer;
    void Start()
    {
        fpsServer = FPSServer.Instance;
    }
    /// <summary>
    /// 开始游戏后，每帧更新玩家数据
    /// </summary>
    public void UpdatePlayer()
    {
        foreach(var p in newPlayer)
        {
            var gc = GCPlayerCmd.CreateBuilder();
            gc.Result = "Add";
            gc.AvatarInfo = p;
            BroadcastToAll(gc);

            var rp = GetPlayer(p.Id);
            if(rp!= null)
            {
                CurrentPlayerToNew(rp);
                CurrentEntityToNew(rp);
            }
        }
        newPlayer.Clear();

        //如果移除报文没有送达 那就是断线了 需要重新同步所有NPC 但是Moba里面不存在移除
        foreach(var p in removePlayer)
        {
            var gc = GCPlayerCmd.CreateBuilder();
            gc.Result = "Remove";
            gc.AvatarInfo = p;
            BroadcastToAll(gc);
        }
        removePlayer.Clear();

        foreach(var p in players)
        {
            var diff = p.GetAvatarInfoDiff();
            if (diff.Changed)
            {
                diff.ClearChanged();
                var gc = GCPlayerCmd.CreateBuilder();
                gc.Result = "Up";
                gc.AvatarInfo = diff.Build();
                BroadcastToAll(gc);
            }

            var posInfo = p.GetPosInfoDiff();
            if (posInfo.Changed)
            {
                var gc = GCPlayerCmd.CreateBuilder();
                gc.Result = "Up";
                gc.AvatarInfo = posInfo.Build();
                BroadcastToAll(gc);
            }
        }
    }

    private FPSPlayer GetPlayer(int id)
    {
        foreach(var p in players)
        {
            if (p.avatarInfo.Id == id) return p;
        }
        return null;
    }

    /// <summary>
    /// TODO:性能优化，根据距离来确定同步频率
    /// </summary>
    public void BroadcastToAll(IBuilderLite cmd)
    {
        foreach(var p in players)
        {
            p.connect.SendPacket(cmd);
        }
    }

    private void CurrentPlayerToNew(FPSPlayer player)
    {
        foreach(var p in players)
        {
            if(p.avatarInfo.Id != player.avatarInfo.Id)
            {
                var info = p.GetAvatarInfo();
                var gc = GCPlayerCmd.CreateBuilder();
                gc.Result = "Add";
                gc.AvatarInfo = info;
                player.SendCmd(gc);
            }
        }
    }
    private void CurrentEntityToNew(FPSPlayer rp)
    {
        var etyCom = GetComponent<FPSEntityManager>();
        etyCom.InitEntityDataToPlayer(rp);
    }
}
