﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyLib;
using SimpleJSON;
using System;

/// <summary>
/// 服务器端Util
/// </summary>
public static class FPSServerUtil
{
    /// <summary>
    /// 加载地图 完成根据角色来初始化每个角色的模型资源等
    /// </summary>
    public static void LoadServerFpsMap()
    {
        WorldManager.worldManager.WorldChangeScene((int)LevelDefine.Battle, false);
        var js = new JSONClass();
        js.Add("total", new JSONData(1));
        RecordData.UpdateRecord(js);
    }

    public static bool IsClientMove(Vector3 clientSpeed)
    {
        return (clientSpeed.x != 0 || clientSpeed.z != 0);
    }


    public static int RealToNetPos(float f)
    {
        return (int)(f * 100);
    }

    public static float GetTimeNow()
    {
        return (float)(DateTime.UtcNow.Ticks / 10000000.0);
    }

    public static double startTime;
    /// <summary>
    /// 距离服务器启动的时间 降低时间大小
    /// </summary>
    public static float GetTimeSinceServerStart()
    {
        return (float)(Util.GetTimeNow() - startTime);
    }
}
