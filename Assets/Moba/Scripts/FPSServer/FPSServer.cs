﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XLua;
using Newtonsoft.Json;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System;
using System.Threading;
using MyLib;
using KBEngine;
using Google.ProtocolBuffers;

/// <summary>
/// 管理FPS 服务器状态
/// 和FPS客户端连接
/// 和FPSMgr连接 主动发起连接
/// </summary>
public class FPSServer : MonoBehaviour
{
    public static FPSServer Instance;
    private FPSPlayerManager playerManager;
    private FPSEntityManager entityManager;
    /// <summary>
    /// 地图加载完成初始化所有玩家模型
    /// </summary>
    public void InitAllPlayer()
    {
        if(state == State.Connected)
        {
        }else
        {
            Debug.LogError("LostConnectToFPSServer");
        }
        state = State.StartGameYet;
        foreach(var p in players)
        {
            ObjectManager.Instance.CreateServerPlayer(p);
        }
        InitEntity();
        StartCoroutine(UpdateWorld());
    }

    /// <summary>
    /// 直接加载Entity对象
    /// </summary>
    private void InitEntity()
    {

    }

    /// <summary>
    /// 不断尝试连接网关确保网关有该服务器
    /// </summary>
    public enum State
    {
        Idle,
        Connecting,
        Connected,
        StartGameYet,//已经有玩家连上开始游戏了
        Closed,
    }
    public State state = State.Idle;

    public List<FPSPlayer> players {
        get
        {
            return playerManager.players;
        }
    }
    //private List<FakeFPSPlayer> fakePlayers = new List<FakeFPSPlayer>();

    public struct LocalInfo
    {
        public string localIp;
        public int localPort;
        public string GetHash()
        {
            return localIp + ":" + localPort;
        }
    }
    /// <summary>
    /// 管理FPS客户端
    /// </summary>
    private FPSTcpServer tcpServer;
    private IMainLoop ml;
    /// <summary>
    /// 连接FPSMgr 连接中央管理器
    /// </summary>
    private RemoteClient rc;
    /// <summary>
    /// 由GateWay 启动游戏服务器
    /// 告知游戏服务器 端口和IP
    /// 游戏服务器通知客户端
    /// 客户端连接真实的房间服务器
    /// 
    /// 读取配置
    /// 告知GateWay
    /// </summary>
    private void Awake()
    {
        Instance = this;
        playerManager = gameObject.AddComponent<FPSPlayerManager>();
        entityManager = gameObject.AddComponent<FPSEntityManager>();

        //建立两个链接
        //从Lua读取配置
        ml = gameObject.AddComponent<MainThreadLoop>();
        //通知GateWay 启动成功
        tcpServer = new FPSTcpServer(ml);
        tcpServer.needAuth = true;
        tcpServer.CheckToken = (c) =>
        {
            foreach(var p in players)
            {
                if(c.authToken == p.avatarInfo.Token)
                {
                    p.SetConnect(c);
                    return true;
                }
            }
            return false;
        };
        tcpServer.OnNewConnect = (c) =>{
        };
        InitInfo();

        //连接TCP管理服务器
    }

    /// <summary>
    /// 每次重连创建新的RemoteClient
    /// </summary>
    private void InitRc()
    {
        rc = new RemoteClient(ml);
        rc.evtHandler = EvtHandler;
        rc.msgHandler = MsgHandler;
        Debug.Log("InitFPSServer");
    }

    /// <summary>
    /// 注册上之后关闭了网关，需要重新注册
    /// </summary>
    /// <param name="evt"></param>
    private void EvtHandler(RemoteClientEvent evt)
    {
        Debug.LogError("FPSServer:Event:"+evt+":"+state);
        if(evt == RemoteClientEvent.Close)
        {
            //需要重新连接
            if(state == State.Connected)
            {
                Debug.LogError("GateWayClosed:ReConnect:");
                state = State.Idle;
                StartCoroutine(RetryConnect());
            }else
            {
                Debug.LogError("LostConnectToGateWay:"+state);
            }
        }
    }


    /// <summary>
    /// FPSMgr 发送的消息处理
    /// 
    /// 获得所有FPSPlayer玩家信息
    /// 还有FPS场景信息
    /// 初始化FPS玩家信息
    /// 装备加载FPS地图，等待玩家切入进来
    /// 本地没有NetMatch 也没有NetWorkScene
    /// </summary>
    /// <param name="packet"></param>
    private void MsgHandler(KBEngine.Packet packet)
    {
        Debug.LogError("FPSServer:MsgHandler:"+packet.protoBody);
        var roomInfo = packet.protoBody as MMFPSRoomInfo;
        //FPSMgr 选择英雄成功
        if(roomInfo.Cmd == "EnterFPSRoom")
        {
            Debug.Log("FPSServer:InitPlayerList:");
            foreach(var info in roomInfo.AvatarInfosList)
            {
                var player = new FPSPlayer();
                player.avatarInfo = info;
                player.Init();
                players.Add(player);
                playerManager.newPlayer.Add(info);
            }

            var ret = MMFPSRoomInfo.CreateBuilder();
            ret.Cmd = "OK";
            rc.SendBuilder(ret, packet.flowId);

            FPSServerUtil.LoadServerFpsMap();
        }
    }

    // Start is called before the first frame update
    /// <summary>
    /// 连接FPS服务器OK再通过TCP连接设置属性
    /// </summary>
    /// <returns></returns>
    IEnumerator Start()
    {
        CreateFPSRoom();
        yield return StartCoroutine(StartServer());
    }

    private LuaTable config;
    private void InitInfo()
    {
        Debug.Log("InitInfo");
        LuaManager.RequireFile("Config");
        config = LuaManager.luaEnv.Global.Get<LuaTable>("InstanceConfig");
        var localPort = config.Get<int>("LocalPort");
        tcpServer.listenPort = localPort;
    }

    /// <summary>
    /// 启动一个TCP服务器
    /// </summary>
    /// <returns></returns>
    private void CreateFPSRoom()
    {
        tcpServer.Init();
    }


    private int tryTimes = 0;
    private float waitTimeInc = 1;
    private float maxWaitTime = 5;

    private IEnumerator RetryConnect()
    {
        tryTimes++;
        Debug.LogError("FPSServer:RetryConnect:"+tryTimes+":"+state);
        state = State.Idle;
        var wt = tryTimes* waitTimeInc;
        wt = Math.Min(wt, maxWaitTime);
        yield return new WaitForSeconds(wt);
        StartCoroutine(StartServer());
    }

    /// <summary>
    /// 启动FPS服务器连接远程的网关
    /// </summary>
    /// <returns></returns>
    private IEnumerator StartServer()
    {
        if (state != State.Idle) yield break;

        state = State.Connecting;
        var ip = config.Get<string>("GateWayIp");
        var port = config.Get<int>("GateWayPort");
        var localPort = config.Get<int>("LocalPort");

        var localInfo = new LocalInfo()
        {
            localIp = "127.0.0.1",
            localPort = localPort,
        };
        Debug.Log("FPSServer:Connect Mgr:"+localPort);

        InitRc();
        rc.Connect(config.Get<string>("GateWayIp"), config.Get<int>("TcpPort")); 
        //连接FPS TCP服务器
        while(rc.state == RemoteClient.State.Connecting)
        {
            yield return null;
        }
        if(rc.state == RemoteClient.State.Closed)
        {
            Debug.LogError("FPSMgr:ConnectFail:");
            state = State.Idle;
            StartCoroutine(RetryConnect());
            yield break;
        }

        state = State.Connected;
        //确定TCP客户端和Http客户端的关系
        var cg = MMFPSRoomInfo.CreateBuilder();
        cg.Token = localInfo.GetHash();
        cg.Cmd = "ServerStart";
        cg.Ip = localInfo.localIp;
        cg.Port = localPort;
        cg.ServerId = config.Get<int>("ServerId");
        KBEngine.Bundle bundle;
        Bundle.GetPacketAckFlow(cg, 0, rc.GetNewFlowId(), out bundle);
       
        yield return StartCoroutine(rc.SendWaitResponse(bundle, (p)=>
        {
        }));
        Debug.Log("FPSServer:Start:ConnectSuccess");
    }
    public bool isStop = false;

    private float roomStartTime = 0;
    private ulong frameId = 0;
    /// <summary>
    /// 处理网络报文的发送和主逻辑刷新
    /// 初始化所有完成
    /// 开始更新地图逻辑
    /// </summary>
    /// <returns></returns>
    private IEnumerator UpdateWorld()
    {
        roomStartTime = FPSServerUtil.GetTimeSinceServerStart();
        frameId = 0;
        var waiter = new WaitForFixedUpdate();
        while(!isStop)
        {
            var gc = GCPlayerCmd.CreateBuilder();
            gc.Result = "SyncFrame";
            gc.FrameId = GetFrameId();
            AddBeforeCmd(gc);
            foreach (var cmd in beforeCmdList)
            {
                playerManager.BroadcastToAll(cmd);
            }
            beforeCmdList.Clear();

            playerManager.UpdatePlayer();
            entityManager.UpdateEntity();

            foreach (var cmd in cmdList)
            {
                playerManager.BroadcastToAll(cmd);
            }
            cmdList.Clear();

            foreach (var cmd in nextFrameCmd)
            {
                playerManager.BroadcastToAll(cmd);
            }
            nextFrameCmd.Clear();
            frameId++;
            yield return waiter;
            yield return waiter;
        }
    }

    private List<IBuilderLite> beforeCmdList = new List<IBuilderLite>();
    private List<IBuilderLite> cmdList = new List<IBuilderLite>();
    private List<IBuilderLite> nextFrameCmd = new List<IBuilderLite>();
    public void AddBeforeCmd(IBuilderLite cmd)
    {
        beforeCmdList.Add(cmd);
    }
    public ulong GetFrameId()
    {
        return frameId;
    }
}
