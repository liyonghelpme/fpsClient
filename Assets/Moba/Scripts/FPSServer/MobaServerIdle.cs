﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyLib;

public class MobaServerIdle : IdleState
{
    public override void EnterState()
    {
        base.EnterState();
        aiCharacter.SetIdle();
    }

    /// <summary>
    /// 接收客户端的输入命令
    /// 进入移动状态
    /// </summary>
    /// <returns></returns>
    public override IEnumerator RunLogic()
    {
        var tempNum = runNum;
        var me = GetPlayer();
        while(CheckInState(tempNum))
        {
            var clientPos = me.GetClientVelocity();
            if (FPSServerUtil.IsClientMove(clientPos))
            {
                aiCharacter.ChangeState(AIStateEnum.MOVE);
                break;
            }
            yield return null;
        }
    }
}
