﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class FPSTcpServer 
{
    /// <summary>
    /// 需要验证权限之后向上层通知新连接
    /// </summary>
    public bool needAuth = true;
    /// <summary>
    /// 上层业务逻辑实现的Auth方法
    /// </summary>
    public System.Func<FPSTcpConnect, bool> CheckToken;
    /// <summary>
    /// 无鉴权的时候创建新连接回调
    /// </summary>
    public System.Action<FPSTcpConnect> OnNewConnect;

    public int listenPort;
    private TcpListener mListener;
    public IMainLoop mainLoop;
    public enum State
    {
        Idle,
        Starting,
        Close,
    }
    private State state = State.Idle;
    public FPSTcpServer(IMainLoop ml)
    {
        mainLoop = ml;
    }

    public void Init()
    {
        state = State.Starting;
        try
        {
            mListener = new TcpListener(IPAddress.Any, listenPort);
            mListener.Server.NoDelay = true;
            mListener.Server.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            mListener.Start(50);
        }
        catch (Exception exception)
        {
            Log.Net("FPStartError:" + exception.ToString());
        }

        //网络处理线程需要 考虑线程安全性 最后都需要跳转到主线程处理回调
        mListener.BeginAcceptTcpClient(new AsyncCallback(AcceptCallback), mListener);
    }

    private void AcceptCallback(IAsyncResult result)
    {
        if (state == State.Starting)
        {
            try
            {
                var listener = (TcpListener)result.AsyncState;
                var socket = listener.EndAcceptTcpClient(result);
                AddAgent(socket);
                listener.BeginAcceptTcpClient(AcceptCallback, listener);
            }
            catch (Exception exp)
            {
                Log.Net("AcceptCallback:" + exp.ToString());
            }
        }
    }

    private void AddAgent(TcpClient socket)
    {
        mainLoop.queueInLoop(()=>
        {
            var c = new FPSTcpConnect(this, socket);
            c.OnNetClose = () =>
            {
                connects.Remove(c);
            };
            connects.Add(c);
            //鉴权之后才上传Connect状态
            if (needAuth)
            {
                c.needAuth = true;
                c.CheckToken = CheckToken;
                c.state = FPSTcpConnect.State.NotAuth;
            }
            else
            {
                if (OnNewConnect != null)
                {
                    OnNewConnect(c);
                }
            }
            c.StartReceive();
        });
    }

    private List<FPSTcpConnect> connects = new List<FPSTcpConnect>();

}
