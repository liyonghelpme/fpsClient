﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyLib;

/// <summary>
/// 服务器上非玩家管理
/// </summary>
public class FPSEntityManager : MonoBehaviour
{
    private List<FPSEntity> entities = new List<FPSEntity>();
    private List<EntityInfo> newEntities = new List<EntityInfo>();
    private List<EntityInfo> removeEntities = new List<EntityInfo>();
    private FPSServer fpsServer;
    void Start()
    {
        fpsServer = GetComponent<FPSServer>();
    }

    /// <summary>
    /// 广播场景所有Entity给客户端
    /// </summary>
    /// <param name="rp"></param>
    internal void InitEntityDataToPlayer(FPSPlayer player)
    {
        foreach(var p in entities)
        {
            var info = p.GetEntityInfo();
            var gc = GCPlayerCmd.CreateBuilder();
            gc.Result = "AddEntity";
            gc.EntityInfo = info;
            player.SendCmd(gc);
        }
    }

    public void UpdateEntity()
    {
        foreach (var p in newEntities)
        {
            var gc = GCPlayerCmd.CreateBuilder();
            gc.Result = "AddEntity";
            gc.EntityInfo = p;
            BroadcastToAll(gc);
        }
        newEntities.Clear();

        foreach (var p in removeEntities)
        {
            var gc = GCPlayerCmd.CreateBuilder();
            gc.Result = "RemoveEntity";
            gc.EntityInfo = p;
            BroadcastToAll(gc);
        }
        removeEntities.Clear();

        foreach (var p in entities)
        {
            var diff = p.GetEntityInfoDiff();
            if (diff.Changed)
            {
                var gc = GCPlayerCmd.CreateBuilder();
                gc.Result = "UpdateEntity";
                gc.EntityInfo = diff;
                BroadcastToAll(gc);
            }
        }
    }

    void BroadcastToAll(GCPlayerCmd.Builder cmd)
    {
        var playerCom = GetComponent<FPSPlayerManager>();
        playerCom.BroadcastToAll(cmd);
    }
}
