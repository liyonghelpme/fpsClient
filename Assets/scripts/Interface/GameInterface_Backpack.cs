﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MyLib
{
    public static class GameInterface_Backpack
    {
        /// <summary>
        /// 拾取某个物品
        /// 本地拾取物品
        /// </summary>
        public static void PickItem(ItemData itemData, int num)
        {
        }

        public static bool BuyItem(int itemId)
        {
            return true;
        }

        public static int GetHpNum()
        {
            var hp = BackPack.backpack.GetHpPotion();
            if (hp == null)
            {
                return 0;
            }
            return hp.num;
        }

        public static void UseItem(int itemId)
        {
            var me = ObjectManager.Instance.GetMyPlayer().GetComponent<AIBase>();
            if (me.GetAI().state.type != AIStateEnum.IDLE)
            {
                me.GetComponent<MyAnimationEvent>().InsertMsg(new MyAnimationEvent.Message(MyAnimationEvent.MsgType.IDLE));
            }
            me.GetComponent<NpcAttribute>().StartCoroutine(UseItemCor(itemId));
        }

        public static void ClearDrug()
        {
            var itemId = (int)ItemData.ItemID.DRUG;
            var backPackId = BackPack.backpack.GetItemId(itemId);
            var count = BackPack.backpack.GetItemCount((int)ItemData.GoodsType.Props, itemId);
            PlayerData.ReduceItem(backPackId, count);
        }

        static System.Collections.IEnumerator UseItemCor(int itemId)
        {
            yield return null;
        }

        public static List<NumMoney> GetChargetList()
        {
            return null;
        }


        public static bool inTransaction = false;
        public static NumMoney lastCharge;

        public static void Charge(NumMoney nm)
        {
            if (inTransaction)
            {
                Debug.LogError("InCharing");
                return;
            }

            inTransaction = true;
            lastCharge = nm;
            SimpleIAP.GetInstance().ChargeItem(nm.itemId);
        }


        /// <summary>
        /// 学习技能书 学习书籍的新技能
        /// 或者增加一个技能点 
        /// </summary>
        /// <param name="propsId">Properties identifier.</param>
        public static void LearnSkillBook(long propsId)
        {
            var backpackData = BackPack.backpack.GetItemInBackPack(propsId);
            if (backpackData != null)
            {
                Log.Sys("LearnForgeSkill " + propsId + " userData " + backpackData.itemData.propsConfig.UserData);
                //backpackData.itemData.propsConfig.
                var skillId = (int)System.Convert.ToSingle(backpackData.itemData.propsConfig.UserData);

                var pinfo = ServerData.Instance.playerInfo;
                var allSkill = pinfo.Skill;
                var find = false;
                foreach (var s in allSkill.SkillInfosList)
                {
                    if (s.SkillInfoId == skillId)
                    {
                        find = true;
                        break;
                    }
                }
            }
        }

        public static void LearnSkill(int skillId)
        {
        }
    }

}