
using UnityEngine;
using System.Collections.Generic;
using Google.ProtocolBuffers;


namespace MyLib {
public partial class Util {
	public delegate IMessageLite MsgDelegate(CodedInputStream buf); 
	

	static IMessageLite GetCGSendChat(CodedInputStream buf) {
		var retMsg = MyLib.CGSendChat.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCSendChat(CodedInputStream buf) {
		var retMsg = MyLib.GCSendChat.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGViewChatGoods(CodedInputStream buf) {
		var retMsg = MyLib.CGViewChatGoods.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCViewChatGoods(CodedInputStream buf) {
		var retMsg = MyLib.GCViewChatGoods.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGLoadMChatShowInfo(CodedInputStream buf) {
		var retMsg = MyLib.CGLoadMChatShowInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCLoadMChatShowInfo(CodedInputStream buf) {
		var retMsg = MyLib.GCLoadMChatShowInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushChat2Client(CodedInputStream buf) {
		var retMsg = MyLib.GCPushChat2Client.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushNotice(CodedInputStream buf) {
		var retMsg = MyLib.GCPushNotice.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushSendNotice(CodedInputStream buf) {
		var retMsg = MyLib.GCPushSendNotice.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGCopyInfo(CodedInputStream buf) {
		var retMsg = MyLib.CGCopyInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCCopyInfo(CodedInputStream buf) {
		var retMsg = MyLib.GCCopyInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCCopyReward(CodedInputStream buf) {
		var retMsg = MyLib.GCCopyReward.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushLevelOpen(CodedInputStream buf) {
		var retMsg = MyLib.GCPushLevelOpen.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGAutoRegisterAccount(CodedInputStream buf) {
		var retMsg = MyLib.CGAutoRegisterAccount.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCAutoRegisterAccount(CodedInputStream buf) {
		var retMsg = MyLib.GCAutoRegisterAccount.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGRegisterAccount(CodedInputStream buf) {
		var retMsg = MyLib.CGRegisterAccount.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCRegisterAccount(CodedInputStream buf) {
		var retMsg = MyLib.GCRegisterAccount.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGLoginAccount(CodedInputStream buf) {
		var retMsg = MyLib.CGLoginAccount.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCLoginAccount(CodedInputStream buf) {
		var retMsg = MyLib.GCLoginAccount.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGCreateCharacter(CodedInputStream buf) {
		var retMsg = MyLib.CGCreateCharacter.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCCreateCharacter(CodedInputStream buf) {
		var retMsg = MyLib.GCCreateCharacter.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGDelCharacter(CodedInputStream buf) {
		var retMsg = MyLib.CGDelCharacter.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCDelCharacter(CodedInputStream buf) {
		var retMsg = MyLib.GCDelCharacter.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGSelectCharacter(CodedInputStream buf) {
		var retMsg = MyLib.CGSelectCharacter.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCSelectCharacter(CodedInputStream buf) {
		var retMsg = MyLib.GCSelectCharacter.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGGetCharacterInfo(CodedInputStream buf) {
		var retMsg = MyLib.CGGetCharacterInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCGetCharacterInfo(CodedInputStream buf) {
		var retMsg = MyLib.GCGetCharacterInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGBindingSession(CodedInputStream buf) {
		var retMsg = MyLib.CGBindingSession.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCBindingSession(CodedInputStream buf) {
		var retMsg = MyLib.GCBindingSession.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGModifyPassword(CodedInputStream buf) {
		var retMsg = MyLib.CGModifyPassword.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCModifyPassword(CodedInputStream buf) {
		var retMsg = MyLib.GCModifyPassword.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGEnterScene(CodedInputStream buf) {
		var retMsg = MyLib.CGEnterScene.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCEnterScene(CodedInputStream buf) {
		var retMsg = MyLib.GCEnterScene.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGPlayerMove(CodedInputStream buf) {
		var retMsg = MyLib.CGPlayerMove.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPlayerMove(CodedInputStream buf) {
		var retMsg = MyLib.GCPlayerMove.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGModifyPlayerName(CodedInputStream buf) {
		var retMsg = MyLib.CGModifyPlayerName.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCModifyPlayerName(CodedInputStream buf) {
		var retMsg = MyLib.GCModifyPlayerName.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGListBranchinges(CodedInputStream buf) {
		var retMsg = MyLib.CGListBranchinges.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCListBranchinges(CodedInputStream buf) {
		var retMsg = MyLib.GCListBranchinges.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGChangeFightMode(CodedInputStream buf) {
		var retMsg = MyLib.CGChangeFightMode.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCChangeFightMode(CodedInputStream buf) {
		var retMsg = MyLib.GCChangeFightMode.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGSettingClothShow(CodedInputStream buf) {
		var retMsg = MyLib.CGSettingClothShow.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCSettingClothShow(CodedInputStream buf) {
		var retMsg = MyLib.GCSettingClothShow.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGSaveGuideStep(CodedInputStream buf) {
		var retMsg = MyLib.CGSaveGuideStep.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCSaveGuideStep(CodedInputStream buf) {
		var retMsg = MyLib.GCSaveGuideStep.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGPropsRevive(CodedInputStream buf) {
		var retMsg = MyLib.CGPropsRevive.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPropsRevive(CodedInputStream buf) {
		var retMsg = MyLib.GCPropsRevive.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGChangeScreen4Point(CodedInputStream buf) {
		var retMsg = MyLib.CGChangeScreen4Point.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCChangeScreen4Point(CodedInputStream buf) {
		var retMsg = MyLib.GCChangeScreen4Point.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGPickUpLootReward(CodedInputStream buf) {
		var retMsg = MyLib.CGPickUpLootReward.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPickUpLootReward(CodedInputStream buf) {
		var retMsg = MyLib.GCPickUpLootReward.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGPickItem(CodedInputStream buf) {
		var retMsg = MyLib.CGPickItem.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGForgotPassword(CodedInputStream buf) {
		var retMsg = MyLib.CGForgotPassword.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCForgotPassword(CodedInputStream buf) {
		var retMsg = MyLib.GCForgotPassword.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushNotice2Kick(CodedInputStream buf) {
		var retMsg = MyLib.GCPushNotice2Kick.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushAttribute2Members(CodedInputStream buf) {
		var retMsg = MyLib.GCPushAttribute2Members.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushLevelUpgrade(CodedInputStream buf) {
		var retMsg = MyLib.GCPushLevelUpgrade.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushAttrChange(CodedInputStream buf) {
		var retMsg = MyLib.GCPushAttrChange.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushExpChange(CodedInputStream buf) {
		var retMsg = MyLib.GCPushExpChange.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushPlayerModifyName(CodedInputStream buf) {
		var retMsg = MyLib.GCPushPlayerModifyName.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushPlayerDressAttributeChanges(CodedInputStream buf) {
		var retMsg = MyLib.GCPushPlayerDressAttributeChanges.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushPlayerPower(CodedInputStream buf) {
		var retMsg = MyLib.GCPushPlayerPower.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushLootReward(CodedInputStream buf) {
		var retMsg = MyLib.GCPushLootReward.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushLootRewardRemove(CodedInputStream buf) {
		var retMsg = MyLib.GCPushLootRewardRemove.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushFightModeChangeWithMap(CodedInputStream buf) {
		var retMsg = MyLib.GCPushFightModeChangeWithMap.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushPlayerResurrect(CodedInputStream buf) {
		var retMsg = MyLib.GCPushPlayerResurrect.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushPlayerToilState(CodedInputStream buf) {
		var retMsg = MyLib.GCPushPlayerToilState.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGGetKeyValue(CodedInputStream buf) {
		var retMsg = MyLib.CGGetKeyValue.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCGetKeyValue(CodedInputStream buf) {
		var retMsg = MyLib.GCGetKeyValue.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGSetKeyValue(CodedInputStream buf) {
		var retMsg = MyLib.CGSetKeyValue.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushNotify(CodedInputStream buf) {
		var retMsg = MyLib.GCPushNotify.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushLevel(CodedInputStream buf) {
		var retMsg = MyLib.GCPushLevel.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGAddProp(CodedInputStream buf) {
		var retMsg = MyLib.CGAddProp.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGSetProp(CodedInputStream buf) {
		var retMsg = MyLib.CGSetProp.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGPlayerCmd(CodedInputStream buf) {
		var retMsg = MyLib.CGPlayerCmd.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPlayerCmd(CodedInputStream buf) {
		var retMsg = MyLib.GCPlayerCmd.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetMMFPSRoomInfo(CodedInputStream buf) {
		var retMsg = MyLib.MMFPSRoomInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetMMHandShake(CodedInputStream buf) {
		var retMsg = MyLib.MMHandShake.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGLoadPackInfo(CodedInputStream buf) {
		var retMsg = MyLib.CGLoadPackInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCLoadPackInfo(CodedInputStream buf) {
		var retMsg = MyLib.GCLoadPackInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGUseUserProps(CodedInputStream buf) {
		var retMsg = MyLib.CGUseUserProps.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCUseUserProps(CodedInputStream buf) {
		var retMsg = MyLib.GCUseUserProps.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGSellUserProps(CodedInputStream buf) {
		var retMsg = MyLib.CGSellUserProps.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCSellUserProps(CodedInputStream buf) {
		var retMsg = MyLib.GCSellUserProps.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGMergeUserProps(CodedInputStream buf) {
		var retMsg = MyLib.CGMergeUserProps.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCMergeUserProps(CodedInputStream buf) {
		var retMsg = MyLib.GCMergeUserProps.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGSplitUserProps(CodedInputStream buf) {
		var retMsg = MyLib.CGSplitUserProps.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCSplitUserProps(CodedInputStream buf) {
		var retMsg = MyLib.GCSplitUserProps.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGAutoAdjustPack(CodedInputStream buf) {
		var retMsg = MyLib.CGAutoAdjustPack.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCAutoAdjustPack(CodedInputStream buf) {
		var retMsg = MyLib.GCAutoAdjustPack.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGLoadShortcutsInfo(CodedInputStream buf) {
		var retMsg = MyLib.CGLoadShortcutsInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCLoadShortcutsInfo(CodedInputStream buf) {
		var retMsg = MyLib.GCLoadShortcutsInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushShortcutsInfo(CodedInputStream buf) {
		var retMsg = MyLib.GCPushShortcutsInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGModifyShortcutsInfo(CodedInputStream buf) {
		var retMsg = MyLib.CGModifyShortcutsInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCModifyShortcutsInfo(CodedInputStream buf) {
		var retMsg = MyLib.GCModifyShortcutsInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGSwapShortcuts(CodedInputStream buf) {
		var retMsg = MyLib.CGSwapShortcuts.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCSwapShortcuts(CodedInputStream buf) {
		var retMsg = MyLib.GCSwapShortcuts.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGUserDressEquip(CodedInputStream buf) {
		var retMsg = MyLib.CGUserDressEquip.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCUserDressEquip(CodedInputStream buf) {
		var retMsg = MyLib.GCUserDressEquip.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGListUserEquip(CodedInputStream buf) {
		var retMsg = MyLib.CGListUserEquip.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCListUserEquip(CodedInputStream buf) {
		var retMsg = MyLib.GCListUserEquip.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGQueryUserEquipInfo(CodedInputStream buf) {
		var retMsg = MyLib.CGQueryUserEquipInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCQueryUserEquipInfo(CodedInputStream buf) {
		var retMsg = MyLib.GCQueryUserEquipInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGPut2Storage(CodedInputStream buf) {
		var retMsg = MyLib.CGPut2Storage.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPut2Storage(CodedInputStream buf) {
		var retMsg = MyLib.GCPut2Storage.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGCheckout4Storage(CodedInputStream buf) {
		var retMsg = MyLib.CGCheckout4Storage.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCCheckout4Storage(CodedInputStream buf) {
		var retMsg = MyLib.GCCheckout4Storage.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGDressCloth(CodedInputStream buf) {
		var retMsg = MyLib.CGDressCloth.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCDressCloth(CodedInputStream buf) {
		var retMsg = MyLib.GCDressCloth.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGTakeOffCloth(CodedInputStream buf) {
		var retMsg = MyLib.CGTakeOffCloth.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCTakeOffCloth(CodedInputStream buf) {
		var retMsg = MyLib.GCTakeOffCloth.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGUnbindingGoods(CodedInputStream buf) {
		var retMsg = MyLib.CGUnbindingGoods.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCUnbindingGoods(CodedInputStream buf) {
		var retMsg = MyLib.GCUnbindingGoods.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushGoodsCountChange(CodedInputStream buf) {
		var retMsg = MyLib.GCPushGoodsCountChange.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushPlayerDressedEquipChange(CodedInputStream buf) {
		var retMsg = MyLib.GCPushPlayerDressedEquipChange.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushPlayerDressInfo(CodedInputStream buf) {
		var retMsg = MyLib.GCPushPlayerDressInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushPackInfo(CodedInputStream buf) {
		var retMsg = MyLib.GCPushPackInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGLevelUpEquip(CodedInputStream buf) {
		var retMsg = MyLib.CGLevelUpEquip.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushEquipDataUpdate(CodedInputStream buf) {
		var retMsg = MyLib.GCPushEquipDataUpdate.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGLevelUpGem(CodedInputStream buf) {
		var retMsg = MyLib.CGLevelUpGem.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGBuyShopProps(CodedInputStream buf) {
		var retMsg = MyLib.CGBuyShopProps.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCBuyShopProps(CodedInputStream buf) {
		var retMsg = MyLib.GCBuyShopProps.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGLoadSkillPanel(CodedInputStream buf) {
		var retMsg = MyLib.CGLoadSkillPanel.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCLoadSkillPanel(CodedInputStream buf) {
		var retMsg = MyLib.GCLoadSkillPanel.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGSkillLevelUp(CodedInputStream buf) {
		var retMsg = MyLib.CGSkillLevelUp.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCSkillLevelUp(CodedInputStream buf) {
		var retMsg = MyLib.GCSkillLevelUp.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGSkillLevelDown(CodedInputStream buf) {
		var retMsg = MyLib.CGSkillLevelDown.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCSkillLevelDown(CodedInputStream buf) {
		var retMsg = MyLib.GCSkillLevelDown.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGLoadInjectPropsLevelUpInfo(CodedInputStream buf) {
		var retMsg = MyLib.CGLoadInjectPropsLevelUpInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCLoadInjectPropsLevelUpInfo(CodedInputStream buf) {
		var retMsg = MyLib.GCLoadInjectPropsLevelUpInfo.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGInjectPropsLevelUp(CodedInputStream buf) {
		var retMsg = MyLib.CGInjectPropsLevelUp.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCInjectPropsLevelUp(CodedInputStream buf) {
		var retMsg = MyLib.GCInjectPropsLevelUp.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetCGResetSkillPoint(CodedInputStream buf) {
		var retMsg = MyLib.CGResetSkillPoint.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCResetSkillPoint(CodedInputStream buf) {
		var retMsg = MyLib.GCResetSkillPoint.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushMemberSkillCD(CodedInputStream buf) {
		var retMsg = MyLib.GCPushMemberSkillCD.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushUnitAddBuffer(CodedInputStream buf) {
		var retMsg = MyLib.GCPushUnitAddBuffer.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushActivateSkill(CodedInputStream buf) {
		var retMsg = MyLib.GCPushActivateSkill.ParseFrom(buf);
		return retMsg;
	}	

	static IMessageLite GetGCPushSkillPoint(CodedInputStream buf) {
		var retMsg = MyLib.GCPushSkillPoint.ParseFrom(buf);
		return retMsg;
	}	


	static Dictionary<string, MsgDelegate> msgMap = new Dictionary<string, MsgDelegate>(){

	{"CGSendChat", GetCGSendChat},

	{"GCSendChat", GetGCSendChat},

	{"CGViewChatGoods", GetCGViewChatGoods},

	{"GCViewChatGoods", GetGCViewChatGoods},

	{"CGLoadMChatShowInfo", GetCGLoadMChatShowInfo},

	{"GCLoadMChatShowInfo", GetGCLoadMChatShowInfo},

	{"GCPushChat2Client", GetGCPushChat2Client},

	{"GCPushNotice", GetGCPushNotice},

	{"GCPushSendNotice", GetGCPushSendNotice},

	{"CGCopyInfo", GetCGCopyInfo},

	{"GCCopyInfo", GetGCCopyInfo},

	{"GCCopyReward", GetGCCopyReward},

	{"GCPushLevelOpen", GetGCPushLevelOpen},

	{"CGAutoRegisterAccount", GetCGAutoRegisterAccount},

	{"GCAutoRegisterAccount", GetGCAutoRegisterAccount},

	{"CGRegisterAccount", GetCGRegisterAccount},

	{"GCRegisterAccount", GetGCRegisterAccount},

	{"CGLoginAccount", GetCGLoginAccount},

	{"GCLoginAccount", GetGCLoginAccount},

	{"CGCreateCharacter", GetCGCreateCharacter},

	{"GCCreateCharacter", GetGCCreateCharacter},

	{"CGDelCharacter", GetCGDelCharacter},

	{"GCDelCharacter", GetGCDelCharacter},

	{"CGSelectCharacter", GetCGSelectCharacter},

	{"GCSelectCharacter", GetGCSelectCharacter},

	{"CGGetCharacterInfo", GetCGGetCharacterInfo},

	{"GCGetCharacterInfo", GetGCGetCharacterInfo},

	{"CGBindingSession", GetCGBindingSession},

	{"GCBindingSession", GetGCBindingSession},

	{"CGModifyPassword", GetCGModifyPassword},

	{"GCModifyPassword", GetGCModifyPassword},

	{"CGEnterScene", GetCGEnterScene},

	{"GCEnterScene", GetGCEnterScene},

	{"CGPlayerMove", GetCGPlayerMove},

	{"GCPlayerMove", GetGCPlayerMove},

	{"CGModifyPlayerName", GetCGModifyPlayerName},

	{"GCModifyPlayerName", GetGCModifyPlayerName},

	{"CGListBranchinges", GetCGListBranchinges},

	{"GCListBranchinges", GetGCListBranchinges},

	{"CGChangeFightMode", GetCGChangeFightMode},

	{"GCChangeFightMode", GetGCChangeFightMode},

	{"CGSettingClothShow", GetCGSettingClothShow},

	{"GCSettingClothShow", GetGCSettingClothShow},

	{"CGSaveGuideStep", GetCGSaveGuideStep},

	{"GCSaveGuideStep", GetGCSaveGuideStep},

	{"CGPropsRevive", GetCGPropsRevive},

	{"GCPropsRevive", GetGCPropsRevive},

	{"CGChangeScreen4Point", GetCGChangeScreen4Point},

	{"GCChangeScreen4Point", GetGCChangeScreen4Point},

	{"CGPickUpLootReward", GetCGPickUpLootReward},

	{"GCPickUpLootReward", GetGCPickUpLootReward},

	{"CGPickItem", GetCGPickItem},

	{"CGForgotPassword", GetCGForgotPassword},

	{"GCForgotPassword", GetGCForgotPassword},

	{"GCPushNotice2Kick", GetGCPushNotice2Kick},

	{"GCPushAttribute2Members", GetGCPushAttribute2Members},

	{"GCPushLevelUpgrade", GetGCPushLevelUpgrade},

	{"GCPushAttrChange", GetGCPushAttrChange},

	{"GCPushExpChange", GetGCPushExpChange},

	{"GCPushPlayerModifyName", GetGCPushPlayerModifyName},

	{"GCPushPlayerDressAttributeChanges", GetGCPushPlayerDressAttributeChanges},

	{"GCPushPlayerPower", GetGCPushPlayerPower},

	{"GCPushLootReward", GetGCPushLootReward},

	{"GCPushLootRewardRemove", GetGCPushLootRewardRemove},

	{"GCPushFightModeChangeWithMap", GetGCPushFightModeChangeWithMap},

	{"GCPushPlayerResurrect", GetGCPushPlayerResurrect},

	{"GCPushPlayerToilState", GetGCPushPlayerToilState},

	{"CGGetKeyValue", GetCGGetKeyValue},

	{"GCGetKeyValue", GetGCGetKeyValue},

	{"CGSetKeyValue", GetCGSetKeyValue},

	{"GCPushNotify", GetGCPushNotify},

	{"GCPushLevel", GetGCPushLevel},

	{"CGAddProp", GetCGAddProp},

	{"CGSetProp", GetCGSetProp},

	{"CGPlayerCmd", GetCGPlayerCmd},

	{"GCPlayerCmd", GetGCPlayerCmd},

	{"MMFPSRoomInfo", GetMMFPSRoomInfo},

	{"MMHandShake", GetMMHandShake},

	{"CGLoadPackInfo", GetCGLoadPackInfo},

	{"GCLoadPackInfo", GetGCLoadPackInfo},

	{"CGUseUserProps", GetCGUseUserProps},

	{"GCUseUserProps", GetGCUseUserProps},

	{"CGSellUserProps", GetCGSellUserProps},

	{"GCSellUserProps", GetGCSellUserProps},

	{"CGMergeUserProps", GetCGMergeUserProps},

	{"GCMergeUserProps", GetGCMergeUserProps},

	{"CGSplitUserProps", GetCGSplitUserProps},

	{"GCSplitUserProps", GetGCSplitUserProps},

	{"CGAutoAdjustPack", GetCGAutoAdjustPack},

	{"GCAutoAdjustPack", GetGCAutoAdjustPack},

	{"CGLoadShortcutsInfo", GetCGLoadShortcutsInfo},

	{"GCLoadShortcutsInfo", GetGCLoadShortcutsInfo},

	{"GCPushShortcutsInfo", GetGCPushShortcutsInfo},

	{"CGModifyShortcutsInfo", GetCGModifyShortcutsInfo},

	{"GCModifyShortcutsInfo", GetGCModifyShortcutsInfo},

	{"CGSwapShortcuts", GetCGSwapShortcuts},

	{"GCSwapShortcuts", GetGCSwapShortcuts},

	{"CGUserDressEquip", GetCGUserDressEquip},

	{"GCUserDressEquip", GetGCUserDressEquip},

	{"CGListUserEquip", GetCGListUserEquip},

	{"GCListUserEquip", GetGCListUserEquip},

	{"CGQueryUserEquipInfo", GetCGQueryUserEquipInfo},

	{"GCQueryUserEquipInfo", GetGCQueryUserEquipInfo},

	{"CGPut2Storage", GetCGPut2Storage},

	{"GCPut2Storage", GetGCPut2Storage},

	{"CGCheckout4Storage", GetCGCheckout4Storage},

	{"GCCheckout4Storage", GetGCCheckout4Storage},

	{"CGDressCloth", GetCGDressCloth},

	{"GCDressCloth", GetGCDressCloth},

	{"CGTakeOffCloth", GetCGTakeOffCloth},

	{"GCTakeOffCloth", GetGCTakeOffCloth},

	{"CGUnbindingGoods", GetCGUnbindingGoods},

	{"GCUnbindingGoods", GetGCUnbindingGoods},

	{"GCPushGoodsCountChange", GetGCPushGoodsCountChange},

	{"GCPushPlayerDressedEquipChange", GetGCPushPlayerDressedEquipChange},

	{"GCPushPlayerDressInfo", GetGCPushPlayerDressInfo},

	{"GCPushPackInfo", GetGCPushPackInfo},

	{"CGLevelUpEquip", GetCGLevelUpEquip},

	{"GCPushEquipDataUpdate", GetGCPushEquipDataUpdate},

	{"CGLevelUpGem", GetCGLevelUpGem},

	{"CGBuyShopProps", GetCGBuyShopProps},

	{"GCBuyShopProps", GetGCBuyShopProps},

	{"CGLoadSkillPanel", GetCGLoadSkillPanel},

	{"GCLoadSkillPanel", GetGCLoadSkillPanel},

	{"CGSkillLevelUp", GetCGSkillLevelUp},

	{"GCSkillLevelUp", GetGCSkillLevelUp},

	{"CGSkillLevelDown", GetCGSkillLevelDown},

	{"GCSkillLevelDown", GetGCSkillLevelDown},

	{"CGLoadInjectPropsLevelUpInfo", GetCGLoadInjectPropsLevelUpInfo},

	{"GCLoadInjectPropsLevelUpInfo", GetGCLoadInjectPropsLevelUpInfo},

	{"CGInjectPropsLevelUp", GetCGInjectPropsLevelUp},

	{"GCInjectPropsLevelUp", GetGCInjectPropsLevelUp},

	{"CGResetSkillPoint", GetCGResetSkillPoint},

	{"GCResetSkillPoint", GetGCResetSkillPoint},

	{"GCPushMemberSkillCD", GetGCPushMemberSkillCD},

	{"GCPushUnitAddBuffer", GetGCPushUnitAddBuffer},

	{"GCPushActivateSkill", GetGCPushActivateSkill},

	{"GCPushSkillPoint", GetGCPushSkillPoint},

	};

	public static IMessageLite GetMsg(int moduleId, int messageId, CodedInputStream buf) {
		//var module = SaveGame.saveGame.getModuleName(moduleId);
		var msg = SaveGame.saveGame.getMethodName(moduleId, messageId);
		Debug.LogWarning ("modulename "+moduleId+" "+messageId+" msg "+msg);

		return msgMap[msg](buf);
	}
}

}
