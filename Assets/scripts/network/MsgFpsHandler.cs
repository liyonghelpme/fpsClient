﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyLib;

public static class MsgFpsHandler
{
    /// <summary>
    /// 同步当前FPS上下文
    /// </summary>
    /// <param name="packet"></param>
    public static void HandleMsg(KBEngine.Packet packet)
    {
        var proto = packet.protoBody as MMFPSRoomInfo;
        var inst = NetMatchScene.Instance;
        var curState = inst.roomState;

        Debug.Log("FPS:HandleMsg:"+packet.protoBody+":"+curState);
        //还没有进入选人阶段
        //客户端数据都准备好了
        //客户端初始化房间信息
        if (proto.Cmd == "InitFPSRoomInfo")
        {
            if (curState == NetMatchScene.RoomState.InMatch
                || curState == NetMatchScene.RoomState.AllReady)
            {
                inst.fpsRoomInfo = proto;
                //inst.roomState = NetMatchScene.RoomState.FPSConnecting;
                //开始建立新的TCP 连接 连接游戏服务器
                inst.fpsClient.StartConnect();
            }
        }
    }
}
