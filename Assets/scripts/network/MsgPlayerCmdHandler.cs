﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MsgPlayerCmdHandler
{
    /// <summary>
    /// 使用和当前场景上下文相关的消息处理模式
    /// </summary>
    public static System.Action<KBEngine.Packet> contextHandler;

    public static void MsgHandler(KBEngine.Packet packet)
    {
        Debug.Log("PlayerCmd:Handle:"+packet.protoBody+":"+contextHandler);
        if(contextHandler != null)
        {
            contextHandler(packet);
        }else
        {
            Debug.LogError("ContextMsgHandlerNotExist:"+packet.protoBody);
        }
    }
}
