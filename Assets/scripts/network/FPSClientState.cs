﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyLib;
using MHType = System.Action<KBEngine.Packet>;
using System;
using KBEngine;
using System.Reflection;

/// <summary>
/// 连接实际的FPS游戏服务器
/// 
/// 管理FPS客户端连接 匹配状态等
/// 每个客户端游戏
/// 
/// 在不同的RC上下文下执行逻辑
/// 尝试实现断线重连的逻辑
/// </summary>
public class FPSClientState : MonoBehaviour
{
    public static FPSClientState Instance;

    public enum State
    {
        Idle,
        WaitInfo,
        Connecting,
        Connected,
        Closed,
        ConnectError,
    }
    public State state = State.Idle;

    private RemoteClient rc;
    public NetMatchScene netMatch;
    private MMFPSRoomInfo fpsRoomInfo;
    private Dictionary<string, MHType> handlers = new Dictionary<string, MHType>();
    void Awake()
    {
        Instance = this;
        var methods = this.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
        foreach (var m in methods)
        {
            var attris = m.GetCustomAttributes(typeof(MsgHandleMethod), false);
            if (attris.Length > 0)
            {
                var action = (MHType)Delegate.CreateDelegate(typeof(MHType), this, m);
                handlers.Add(m.Name, action);
            }
        }
    }
    /// <summary>
    /// 服务器管理器通知连接FPS服务器
    /// </summary>
    public void StartConnect()
    {
        Debug.Log("StartConnect:Fps:Server:"+fpsRoomInfo);
        StartCoroutine(ConnectFPSServer());
    }

    /// <summary>
    /// 连接FPS服务器
    /// 连接成功之后才可以选择英雄
    /// </summary>
    /// <returns></returns>
    private IEnumerator ConnectFPSServer()
    {
        Debug.Log("ConnectFPSServer");
        if (state == State.Idle)
        {
            fpsRoomInfo = netMatch.fpsRoomInfo;
            state = State.Connecting;

            //连接FPS服务器处理网络报文
            rc = new RemoteClient(netMatch.ml);
            rc.evtHandler = EvtHandler;
            rc.msgHandler = MsgHandler;
            //MsgPlayerCmdHandler.contextHandler = MsgHandler;

            rc.Connect(fpsRoomInfo.Ip, fpsRoomInfo.Port);
            //等待连接游戏服务器
            while (lastEvt == RemoteClientEvent.None && state == State.Connecting)
            {
                yield return null;
            }

            //连接FPS服务器成功
            //TODO:连接服务器失败断线重新启动新的连接状态
            if (lastEvt == RemoteClientEvent.Connected)
            {
                state = State.Connected;
            }else
            {
                Debug.LogError("FPS Server Connect Fail");
                state = State.ConnectError;
            }

            Debug.Log("ConnectFPSServer:Suc:"+fpsRoomInfo);
        }
    }

    private RemoteClientEvent lastEvt = RemoteClientEvent.None;
    void EvtHandler(RemoteClientEvent evt)
    {
        Debug.LogError("FpsClientState:EvtHandler:"+evt);
        lastEvt = evt; 
        if(evt == RemoteClientEvent.Close)
        {
            //if (state != State.Closed)
            //需要断线重连机制或者 返回到匹配界面
            if(state == State.Connected)
            {
                state = State.Closed;
                QuitScene();
            }
        }
    }

    //返回到匹配场景
    private void QuitScene()
    {
        Log.Sys("QuitScene");
        WorldManager.worldManager.WorldChangeScene(2, false);
        ClearState();
    }

    private void ClearState()
    {
        state = State.Idle;
        rc = null;
    }

    /// <summary>
    /// 替代NetMatch 和 NetScene 场景的协议处理
    /// </summary>
    /// <param name="packet"></param>
    void MsgHandler(KBEngine.Packet packet)
    {
        Debug.Log("FPSClientState:MsgHandle:"+packet.protoBody);
        var cmd = packet.protoBody as GCPlayerCmd;
        var cmds = cmd.Result.Split(' ');
        var cmd0 = cmds[0];
        if (handlers.ContainsKey(cmd0))
        {
            handlers[cmd0](packet);
        }
        else
        {
            Debug.LogError("FPSClientState:NotFind:"+packet.protoBody+":"+cmd0);
        }
    }



    private ulong ServerFrameId = 0;
    private float lastSyncTime = 0;
    [MsgHandleMethod]
    private void SyncFrame(Packet p)
    {
        var proto = p.protoBody as GCPlayerCmd;
        ServerFrameId = proto.FrameId;
        lastSyncTime = Time.time;
    }

    [MsgHandleMethod]
    private void Up(Packet p)
    {
        var proto = p.protoBody as GCPlayerCmd;
        var player = ObjectManager.Instance.GetPlayer(proto.AvatarInfo.Id);
        if (player != null)
        {
            var sync = player.GetComponent<ISyncInterface>();
            if (sync != null)
            {
                sync.NetworkAttribute(proto);
            }
        }
    }
    [MsgHandleMethod]
    private void Remove(Packet p)
    {
        var proto = p.protoBody as GCPlayerCmd;
        ObjectManager.Instance.DestroyPlayer(proto.AvatarInfo.Id);
    }

    [MsgHandleMethod]
    private void AllReady(Packet p)
    {
        Debug.Log("AllReady");
        NetMatchScene.Instance.SetAllReady();
    }

    /// <summary>
    /// 客户端请求 进入FPS游戏
    /// 根据自己的Token用于FPS 服务器Match ClientConnect 和 FPSMgr上发送的信息配对
    /// 
    /// 三个对象互相通知：
    /// 1：FPSServer
    /// 2：FPSClient
    /// 3：GameServerManager
    /// 选择完英雄进入FPS游戏
    /// 服务器打包选择英雄状态通知FPSServer
    /// 客户端尝试进入FPSServer
    /// </summary>
    public void EnterFPSGame()
    {
        Debug.Log("FPSClientstate:EnterFPSGame:"+state);
        if(state == State.Connected)
        {
            var gc = MMHandShake.CreateBuilder();
            gc.Token = netMatch.myToken;
            Debug.Log("HandShakeFPSServer:"+gc);
            //进入FPS服务器成功
            StartCoroutine(rc.SendWaitCmd(gc, (p)=>
            {
                Debug.Log("EnterFPSGame:"+p.protoBody);
                var shake = p.protoBody as MMHandShake;
                if(shake.Success)
                {
                    netMatch.BeginLoadClientMap();
                }
            }));
        }
    }

    /// <summary>
    /// 本地地图加载OK
    /// </summary>
    public void SendReady()
    {
        var cg = CGPlayerCmd.CreateBuilder();
        cg.Cmd = "Ready";
        rc.SendBuilder(cg);
    }
      
    /// <summary>
    ///  初始化FPS玩家的信息
    /// </summary>
    /// <returns></returns>
    public IEnumerator SendUserData()
    {
        Debug.LogError("SendUserData: " + state + " rc " + rc);
        var me = ObjectManager.Instance.GetMyPlayer();
        var pos = me.transform.position;
        var dir = (int)me.transform.localRotation.eulerAngles.y;

        var cg = CGPlayerCmd.CreateBuilder();
        cg.Cmd = "InitData";
        var ainfo = AvatarInfo.CreateBuilder();
        ainfo.X = (int)(pos.x * 100);
        ainfo.Z = (int)(pos.z * 100);
        ainfo.Y = (int)(pos.y * 100);
        ainfo.Dir = dir;
        ainfo.Name = ServerData.Instance.playerInfo.Roles.Name;

        ainfo.Level = ObjectManager.Instance.GetMyProp(CharAttribute.CharAttributeEnum.LEVEL);
        ainfo.HP = ObjectManager.Instance.GetMyProp(CharAttribute.CharAttributeEnum.HP);
        ainfo.Job = ServerData.Instance.playerInfo.Roles.Job;

        cg.AvatarInfo = ainfo.Build();
        var sync = me.GetComponent<MeSyncToServer>();
        sync.InitData(cg.AvatarInfo);

        yield return StartCoroutine(rc.SendWaitCmd(cg, (p) =>
        {
        }));
        WindowMng.windowMng.ShowNotifyLog("玩家数据初始化成功");
    }



    /// <summary>
    /// 周期性的同步属性状态到服务器上面 Diff属性
    /// lastAvatarInfo 比较后的属性 
    /// 操作命令在彻底和服务器同步之后开始发送
    /// </summary>
    /// <returns>The command to server.</returns>
    public IEnumerator SendCommandToServer()
    {
        Debug.LogError("SendCommandToServer");
        var waitTime = new WaitForSeconds(ClientApp.Instance.syncFreq);

        //客户端每隔100ms向服务器同步一次操控位置信息
        //等待服务器通知AllReady才开始设置属性
        while (NetMatchScene.Instance.roomState != NetMatchScene.RoomState.AllReady)
        {
            Debug.Log("WaitRoomState:"+NetMatchScene.Instance.roomState);
            yield return null;
        }

        var me = ObjectManager.Instance.GetMyPlayer();
        if (me != null)
        {
            var sync = me.GetComponent<ISyncInterface>();
            while (!sync.CheckSyncState())
            {
                yield return null;
            }
        }
        while (state == State.Connected)
        {
            SyncMyAttribute();
            SyncUDPPos();
            yield return waitTime;
        }
    }

    private void SyncMyAttribute()
    {
        var me = ObjectManager.Instance.GetMyPlayer();
        if (me != null)
        {
            var sync = me.GetComponent<MeSyncToServer>();
            sync.SyncAttribute();
        }
    }

    private void SyncUDPPos()
    {
        var me = ObjectManager.Instance.GetMyPlayer();
        if (me != null)
        {
            var sync = me.GetComponent<MeSyncToServer>();
            sync.SyncPos();
        }
    }

    public void SendCmd(Google.ProtocolBuffers.IBuilderLite cmd)
    {
        rc.SendBuilder(cmd);
    }
}
