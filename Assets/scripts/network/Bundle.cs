﻿
/*
Author: liyonghelpme
Email: 233242872@qq.com
*/
using Google.ProtocolBuffers;
using System.Collections.Concurrent;

namespace KBEngine
{
  	using UnityEngine; 
	using System; 
	using System.Collections;
	using System.Collections.Generic;
	using MyLib;
    /// <summary>
    /// 多线程共享对象
    /// </summary>
    public class Bundle 
    {
        public bool inUse = false;
        private static ConcurrentQueue<Bundle> pool = new ConcurrentQueue<Bundle>();

        private static Bundle GetBundle()
        {
            Bundle q = null;
            var has = pool.TryDequeue(out q);
            if (has)
            {
                q.inUse = true;
                return q;
            }
            var nb = new Bundle();
            nb.inUse = true;
            return nb;
        }

        public static void ReturnBundle(Bundle bundle)
        {
            if (!bundle.inUse) return;
            bundle.inUse = false;
            bundle.Reset();
            pool.Enqueue(bundle);
        }

        private void Reset()
        {
            moduleId = 0;
            msgId = 0;
            numMessage = 0;
            //stream.clear();
            coutStream.ClearBuffer();
        }

        public static List<string> sendMsg = new List<string>();
		public static List<string> recvMsg = new List<string> ();
		public int numMessage = 0;

		public byte moduleId;
		public byte msgId;
        public byte ackId = 0;
        public byte flowId1 = 0;
        public IMessageLite protoMsg = null;
		public Bundle()
		{
            coutStream = CodedOutputStream.CreateInstance(new byte[8192]);
		}

		public void newMessage(System.Type type) {
#if DEBUG
			sendMsg.Add("Bundle:: 开始发送消息 Message is " + type.Name+" ");
			if (sendMsg.Count > 30) {
				sendMsg.RemoveRange(0, sendMsg.Count-30);
			}

            Log.Net ("Bundle:: 开始发送消息 Message is " + type.Name);
#endif

			var pa = Util.GetMsgID (type.Name);
			moduleId = pa.moduleId;
			msgId = pa.messageId;
			
			numMessage += 1;
		}

        public static void GetPacketAckFlow(IBuilderLite build, byte ackId, byte flowId, out Bundle b)
        {
            var data = build.WeakBuild();
            var bundle = GetBundle();
            bundle.newMessage(data.GetType());
            bundle.ackId = ackId;
            bundle.flowId1 = flowId;
            bundle.writePB(data);

            b = bundle;
        }

        public CodedOutputStream coutStream;
        public int totalBundleLen = 0;

		/*
		 * 0xcc   int8
		 * length int32
		 * flowId int32
		 * moduleId int8
		 * messageId int16
		 * protobuffer
		 */ 
		public void writePB(IMessageLite pbMsg) {
            protoMsg = pbMsg;

            coutStream.ClearBuffer();
            coutStream.WriteRawLittleEndian16(0);
            var headLen = coutStream.position;
            coutStream.WriteRawByte(ackId);
            coutStream.WriteRawByte(flowId1);
            coutStream.WriteRawByte(moduleId);
            coutStream.WriteRawByte(msgId);
            pbMsg.WriteTo(coutStream);
            var totalLen = coutStream.position;
            var bodyLen = totalLen - headLen;
            coutStream.position = 0;
            coutStream.WriteRawLittleEndian16((uint)bodyLen);
            coutStream.position = totalLen;
            totalBundleLen = totalLen;
		}

    }
} 
