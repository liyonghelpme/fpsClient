﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MyLib;

/// <summary>
/// 所有网络通信处理
/// </summary>
public static class GeneralMsgHandler 
{
    private static Dictionary<System.Type, System.Action<KBEngine.Packet>> handlers = new Dictionary<Type, Action<KBEngine.Packet>>();

    /// <summary>
    /// 协议类型
    /// 协议处理函数
    /// </summary>
    public static void RegisterHandler(System.Type tp, System.Action<KBEngine.Packet> handler)
    {
        handlers.Add(tp, handler);
    }

    public static void MsgHandler(KBEngine.Packet packet)
    {
        Debug.Log("MsgHandler:"+packet.protoBody);
        var tp = packet.protoBody.GetType();
        if (handlers.ContainsKey(tp))
        {
            handlers[tp](packet);
        }
        else
        {
            Debug.LogError("NotFindMsgHandler:"+packet.protoBody);
        }
    }

    public static void InitAllProtocol()
    {
        RegisterHandler(typeof(GCPlayerCmd), MsgPlayerCmdHandler.MsgHandler);
        RegisterHandler(typeof(MMFPSRoomInfo), MsgFpsHandler.HandleMsg);
    }
}
