﻿/*
Author: liyonghelpme
Email: 233242872@qq.com
*/

using MyLib;
using UnityEngine;
using System.Collections;
using KBEngine;
using System;

public class ClientApp : UnityEngine.MonoBehaviour
{
    public static KBEngineApp gameapp = null;
    public static ClientApp Instance;
    public enum ClientState
    {
        Idle,
        InInit,
        FinishInit,
    }
    public ClientState state = ClientState.Idle;
    public string remoteServerIP;
    public int remotePort = 10001;
    /// <summary>
    /// 同步客户端操作到服务器上的频率
    /// </summary>
    public float syncFreq = 0.1f;

    public int remoteUDPPort = 10001;
    public int UDPListenPort = 10002;

    public int ServerHttpPort = 12002;
    public int remoteKCPPort = 6060;

    public bool testAI = false;
    /// <summary>
    /// 通过配置或者启动命令 .ini文件 或者Lua文件 Config.lua
    /// 编辑器中 已经启动Idle FPSServer
    /// 新启动的服务器实例 编译好的Server
    /// </summary>
    public enum InstMode
    {
        Client,
        Server,
    }
    public InstMode mode = InstMode.Client;
    public static bool IsClient
    {
        get
        {
            return Instance.mode == InstMode.Client;
        }
    }

    /// <summary>
    /// 默认请求的更新服务器IP
    /// 服务器模式不需要
    /// </summary>
    public string QueryServerIP = "127.0.0.1";
   
    void Awake()
    {
        FPSServerUtil.startTime = FPSServerUtil.GetTimeNow();
        Instance = this;
        DontDestroyOnLoad(this.gameObject);
        StopLog();
        //加载配置之类的
    }
    /// <summary>
    /// 是否打印日志
    /// </summary>
    void StopLog()
    {
        //Debug.unityLogger.logEnabled = false;
    }
    /// <summary>
    /// 初始化资源加载和Lua加载
    /// 初始化连接服务器
    /// CheckUpdate
    /// AssetBundle Loader初始化
    /// 加载LuaAB
    /// </summary>
    public void AfterInitServer()
    {
        StartCoroutine(InitBeforeLogin());
    }
    private IEnumerator InitBeforeLogin()
    {
        state = ClientState.InInit;
        yield return HotUpdateManager.Instance.StartCoroutine(HotUpdateManager.Instance.CheckUpdate());
        yield return StartCoroutine(ABLoader.Instance.InitLoader());
        yield return StartCoroutine(ABLoader.Instance.LoadLuaAb());
        LuaManager.Instance.InitLua();
        state = ClientState.FinishInit;

        if (mode == InstMode.Server)
        {
            var go = new GameObject("_FPSServer");
            GameObject.DontDestroyOnLoad(go);
            go.AddMissingComponent<FPSServer>();
        }
        Debug.LogError("InitAllFinish");
    }

    public void StartServer() {
        Debug.Log("StartServer");
    }
    // Use this for initialization
    void Start()
    {
        UnityEngine.MonoBehaviour.print("client app start");
        gameapp = new KBEngineApp(this);
    }


    void OnDestroy()
    {
        UnityEngine.MonoBehaviour.print("clientapp destroy");
        if (KBEngineApp.app != null)
        {
            UnityEngine.MonoBehaviour.print("client app over " +  " over = ");
        }
    }

    void Update()
    {
        KBEUpdate();
    }

    //处理网络数据
    void KBEUpdate()
    {
        //处理网络回调
        gameapp.UpdateMain();
    }
    public bool IsPause = false;
    public void OnApplicationPause(bool pauseStatus) {
        IsPause = pauseStatus;
        if(pauseStatus && MyLib.ServerData.Instance != null){
            //ChuMeng.DemoServer.demoServer.GetThread().CloseServerSocket();
            MyLib.ServerData.Instance.SaveUserData();
        }
        if (pauseStatus)
        {
            var act = WorldManager.worldManager.GetActive();
            if (act != null && !act.IsCity)
            {
                WorldManager.ReturnCity();
            }
            StatisticsManager.Instance.QuitGame();
        }
    }

    [ButtonCallFunc()]
    public bool PauseTest;

    public void PauseTestMethod()
    {
        OnApplicationPause(true);
    }

}
