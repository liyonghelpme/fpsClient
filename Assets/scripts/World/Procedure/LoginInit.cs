﻿/*
Author: liyonghelpme
Email: 233242872@qq.com
*/
using UnityEngine;
using System.Collections;
using KBEngine;
using Google.ProtocolBuffers;

namespace MyLib
{
    /// <summary>
    /// 客户端初始化之后， 接着初始化登陆界面  如果是客户模式
    /// 支持两种模式
    /// Server
    /// Client
    /// </summary>
	public class LoginInit : UnityEngine.MonoBehaviour
	{
		static LoginInit Instance;

	    public bool testNormalLogin;

		public static LoginInit GetLogin() {
			return Instance;
		}
		void Awake ()
		{
			Instance = this;

			if (SaveGame.saveGame == null) {
				var saveGame = new GameObject("SaveGame");
				saveGame.AddComponent<SaveGame>();
				saveGame.GetComponent<SaveGame>().InitData();
				saveGame.GetComponent<SaveGame>().InitServerList();
			}
		    var sd = new ServerData();
            sd.LoadData();
		}


        /// <summary>
        /// ClientApp初始化完成
        /// Logininit 初始化完成
        /// </summary>
		void Start ()
		{
            //客户端启动登陆界面
            if (ClientApp.Instance.mode == ClientApp.InstMode.Client)
            {
                WindowMng.windowMng.PushView("UI/loginUI2");
                MyEventSystem.myEventSystem.PushEvent(MyEvent.EventType.UpdateLogin);
            }else //连接FPS GateWay服务器
            {
                //var go = new GameObject("_FPSServer");
                //GameObject.DontDestroyOnLoad(go);
                //go.AddMissingComponent<FPSServer>();
                ClientApp.Instance.AfterInitServer();
            }
		}
	}

}
