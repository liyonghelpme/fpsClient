﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyLib
{
    /// <summary>
    /// Moba战斗场景地图
    /// </summary>
    public class Map8 : CScene
    {
        public override bool ShowTeamColor
        {
            get
            {
                return false;
            }
        }

        public override bool IsEnemy(GameObject a, GameObject b)
        {
            var aattr = NetworkUtil.GetAttr(a);
            var battr = NetworkUtil.GetAttr(b);
            if (aattr != null && battr != null)
            {
                return a != b && b.tag == GameTag.Player && aattr.TeamColor != battr.TeamColor;
            }
            return false;
        }

        public override bool IsNet
        {
            get
            {
                return true;
            }
        }

        public override bool IsRevive
        {
            get
            {
                return true;
            }
        }


        private NetworkScene netScene;

        protected override void Awake()
        {
            base.Awake();
            //客户端模式
            if (ClientApp.Instance.mode == ClientApp.InstMode.Client)
            {
                netScene = gameObject.AddComponent<NetworkScene>();
                gameObject.AddComponent<NetworkLatency>();
            }else //服务器模式
            {

            }
        }

        private void Start()
        {
            if (ClientApp.IsClient)
            {
                gameObject.AddComponent<ScoreManager>();
                var newBee = PlayerPrefs.GetInt("NewBee", 0);
                //var newBee = 0;
                if (newBee == 0)
                {
                    StartCoroutine(Util.WaitCb(5, () =>
                    {
                        newBee = 1;
                        PlayerPrefs.SetInt("NewBee", 1);
                    //WindowMng.windowMng.PushView("UI/Newbee", false, false);
                    var uiMain = WindowMng.windowMng.GetMainUI();
                        WindowMng.windowMng.AddChild(uiMain, "UI/Newbee");
                    }));
                }
                StartCoroutine(WaitInitMap());
            }else
            {
                StartCoroutine(WaitServerInit());
            }
        }

        private IEnumerator WaitServerInit()
        {
            yield return StartCoroutine(NetworkUtil.WaitForWorldInit());
            FPSServer.Instance.InitAllPlayer();
        }
        /// <summary>
        /// 等待世界初始化结束 再初始化坦克网络
        /// </summary>
        /// <returns></returns>
        private IEnumerator WaitInitMap()
        {
            yield return StartCoroutine(NetworkUtil.WaitForWorldInit());
            netScene.InitMap();
        }
        public override void BroadcastMsg(CGPlayerCmd.Builder cmd)
        {
            /*
            if (state == SceneState.InGame)
            {
                netScene.BroadcastMsg(cmd);
            }
            */
            NetworkUtil.Broadcast(cmd);
        }

        public override void BroadcastMsgUDP(CGPlayerCmd.Builder cmd)
        {
            if (state == SceneState.InGame)
            {
                netScene.BroadcastUDPMsg(cmd);
            }
        }
        public override void BroadcastKCP(CGPlayerCmd.Builder cmd)
        {
            if (state == SceneState.InGame)
            {
                netScene.BroadcastKCPMsg(cmd);
            }
        }
    }

}
